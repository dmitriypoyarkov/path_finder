#ifndef GLOBAL_PLANNER_CPP
#define GLOBAL_PLANNER_CPP

namespace global_planner
{
    class GlobalPlanner : public nav_core::BaseGlobalPlanner
    {
    public:
        GlobalPlanner();
        GlobalPlanner(std::string name, costmap_2d::Costmap2D *costmap, std::string frame_id);

        /** overridden classes from interface nav_core::BaseGlobalPlanner **/
        void initialize(std::string name, costmap_2d::Costmap2DROS *costmap_ros);
        void initialize(std::string name, costmap_2d::Costmap2D *costmap, std::string frame_id);
        bool makePlan(const geometry_msgs::PoseStamped &start,
                      const geometry_msgs::PoseStamped &goal,
                      std::vector<geometry_msgs::PoseStamped> &plan);

        bool currentPlanContains(const geometry_msgs::PoseStamped &point, geometry_msgs::PoseStamped &near_point_out);

    protected:
        /**
   * @brief Store a copy of the current costmap in \a costmap.  Called by makePlan.
   */
        void publishPlan(const std::vector<geometry_msgs::PoseStamped> &path, const ros::Publisher &pub);
        geometry_msgs::PoseStamped getStartPose();
        void replacePointInPlan(geometry_msgs::PoseStamped existingPoint,
                                geometry_msgs::PoseStamped newPoint);
        bool findBackPoint(const geometry_msgs::Point &initialPoint,
                           std::vector<geometry_msgs::PoseStamped> &path,
                           geometry_msgs::Point &outBackPoint) const;
        float calculateAverageStep(const std::vector<geometry_msgs::PoseStamped> &path) const;
        bool tryInsertSidePoint(std::vector<geometry_msgs::PoseStamped> &path, const geometry_msgs::Point &point);

        costmap_2d::Costmap2D *costmap_;
        std::string frameId_;
        ros::Publisher planPub_;
        ros::Publisher fullPlanPub_;
        ros::Publisher markerPub_;
        ros::ServiceClient planClient_;
        tf::StampedTransform startPosTf_;
        tf::TransformListener startPosListener_;

        std::vector<geometry_msgs::PoseStamped> currentPlan_;
        std::vector<geometry_msgs::PoseStamped> fullPlan_;

        bool initialized_;

        static const float SQR_TOLERANCE;
    };
};



#endif