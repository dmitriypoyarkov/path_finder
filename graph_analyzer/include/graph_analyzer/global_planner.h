/** include the libraries you need in your planner here */
/** for global path planner interface */
#include <ros/ros.h>
#include <costmap_2d/costmap_2d_ros.h>
#include <costmap_2d/costmap_2d.h>
#include <nav_core/base_global_planner.h>
#include <geometry_msgs/PoseStamped.h>
#include <angles/angles.h>
#include <nav_msgs/Path.h>
#include <base_local_planner/world_model.h>
#include <base_local_planner/costmap_model.h>
#include <tf/transform_listener.h>

using std::string;

#ifndef GLOBAL_PLANNER_CPP
#define GLOBAL_PLANNER_CPP

namespace global_planner
{
    class GlobalPlanner : public nav_core::BaseGlobalPlanner
    {
    public:
        GlobalPlanner();
        GlobalPlanner(std::string name, costmap_2d::Costmap2D *costmap, std::string frame_id);

        /** overridden classes from interface nav_core::BaseGlobalPlanner **/
        void initialize(std::string name, costmap_2d::Costmap2DROS *costmap_ros);
        void initialize(std::string name, costmap_2d::Costmap2D *costmap, std::string frame_id);
        bool makePlan(const geometry_msgs::PoseStamped &start,
                      const geometry_msgs::PoseStamped &goal,
                      std::vector<geometry_msgs::PoseStamped> &plan);

    protected:
        /**
   * @brief Store a copy of the current costmap in \a costmap.  Called by makePlan.
   */
        void publishPlan(const std::vector<geometry_msgs::PoseStamped> &path, const ros::Publisher &pub);
        costmap_2d::Costmap2D *costmap_;
        std::string frameId_;
        ros::Publisher planPub_;
        ros::Publisher markerPub_;
        ros::ServiceClient planClient_;

        bool initialized_;

        static const float SQR_TOLERANCE;
    };
};
#endif
