#pragma once
#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <tf/tf.h>

namespace global_planner
{
    class MyMath
    {
    public:
        static float pointSqrLenght(geometry_msgs::Pose pose)
        {
            return pow(pose.position.x, 2) + pow(pose.position.y, 2) + pow(pose.position.z, 2);
        }

        static float sqrDistanceBetweenPoints(const geometry_msgs::Point &point1, const geometry_msgs::Point &point2)
        {
            return pow(point1.x - point2.x, 2) + pow(point1.y - point2.y, 2) +
                   pow(point1.z - point2.z, 2);
        }

        static geometry_msgs::Point projectPointOnLine(const geometry_msgs::Point point, const geometry_msgs::Point linePoint1,
                                                       const geometry_msgs::Point linePoint2)
        {
            auto x0 = point.x;
            auto y0 = point.y;
            auto x1 = linePoint1.x;
            auto y1 = linePoint1.y;
            auto xb = linePoint2.x;
            auto yb = linePoint2.y;

            if (linePoint1 == linePoint2)
            {
                ROS_ERROR("GLOBAL PLANNER: projectPointOnLine: points are same; aborting");
                exit(1);
            }
            if (linePoint1 == point)
            {
                return linePoint1;
            }
            if (linePoint2 == point)
            {
                return linePoint2;
            }

            auto m = xb - x1;
            auto p = yb - y1;

            auto x = (x0 * pow(m, 2) + p * (x1 * p + m * (y0 - y1))) / (pow(m, 2) + pow(p, 2));
            auto y = (x0 * m * p - x1 * m * p + y0 * pow(p, 2) + pow(m, 2) * y1) / (pow(m, 2) + pow(p, 2));

            auto projectedPoint = geometry_msgs::Point();
            projectedPoint.x = x;
            projectedPoint.y = y;
            return projectedPoint;
        }

        // for correct work basePoint must be between point1 and point2
        static bool pointsAreOnDifferentSides(const geometry_msgs::Point &basePoint, const geometry_msgs::Point &point1,
                                              const geometry_msgs::Point &point2)
        {
            if (point1 == point2)
            {
                ROS_ERROR("GLOBAL PLANNER: pointsAreOnDifferentSides: points are same! aborting");
            }
            auto relative1 = substractPoints(point1, basePoint);
            auto relative2 = substractPoints(point2, basePoint);
            auto cond1 = relative1.x >= 0 && relative2.x <= 0;
            auto cond2 = relative1.x <= 0 && relative2.x >= 0;
            auto cond3 = relative1.y >= 0 && relative2.y <= 0;
            auto cond4 = relative1.y <= 0 && relative2.y >= 0;
            if ((cond1 || cond2) && (cond3 || cond4))
            {
                return true;
            }
            return false;
        }

        static geometry_msgs::Point substractPoints(geometry_msgs::Point point1, geometry_msgs::Point point2)
        {
            auto result = geometry_msgs::Point();
            result.x = point1.x - point2.x;
            result.y = point1.y - point2.y;
            result.z = point1.z - point2.z;
            return result;
        }

        static float vectorLength2D(const geometry_msgs::Point &vec)
        {
            if (vec.z != 0)
            {
                ROS_ERROR("vector2D: vector z is not zero!");
            }
            return sqrt(pow(vec.x, 2) + pow(vec.y, 2));
        }

        static float vectorDotProduct2D(const geometry_msgs::Point &vec1, const geometry_msgs::Point &vec2)
        {
            if (vec1.z != 0 || vec2.z != 0)
            {
                ROS_ERROR("vector2D: vector z is not zero!");
            }
            return vec1.x * vec2.x + vec1.y * vec2.y + vec1.z * vec2.z;
        }

        static float vectorAngle2D(const geometry_msgs::Point &vec)
        {
            if (vec.x == 0 && vec.y == 0)
            {
                return 0;
            }
            float angle = atan2(vec.y, vec.x);

            // auto vecLength = vectorLength2D(vec);
            // auto verticalVector = geometry_msgs::Point();
            // verticalVector.x = 1;
            // verticalVector.y = 0;
        
            // auto vecsDotProduct = vectorDotProduct2D(vec, verticalVector);
            // // auto cosBetweenVectors = vecsDotProduct / (vecLength);
            // auto cosBetweenVectors = vec.x / vecLength;

            // float angle = 0;
            // if (vec.y > 0)
            // {
            //     angle = acos(cosBetweenVectors);
            // }
            // if (vec.y <= 0)
            // {
            //     angle = -acos(cosBetweenVectors);
            // }
            return angle;
        }

        static float angleBetweenVectors2D(const geometry_msgs::Point &vec1, const geometry_msgs::Point &vec2)
        {
            return vectorAngle2D(vec1) - vectorAngle2D(vec2);
        }

        static geometry_msgs::Quaternion vectorQuaternion2D(const geometry_msgs::Point &vec)
        {
            auto angle = vectorAngle2D(vec);
            auto quaternion_tf = tf::createQuaternionFromRPY(0,0,angle);
            auto quaternion_gmsgs = geometry_msgs::Quaternion();
            tf::quaternionTFToMsg(quaternion_tf, quaternion_gmsgs);
            
            return quaternion_gmsgs;
        }
    };
}