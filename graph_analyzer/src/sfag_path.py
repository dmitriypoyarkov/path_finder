from typing import List, Tuple, Union

from cell import Cell
from geometry_msgs.msg import Point


class SFAGPath(object):
    def __init__(self, id : Union[int, None], cell_path : List[Cell], direction : int,
            parent_id : Union[int, None]):
        self._id = id
        self._cell_path = cell_path
        self._direction = direction
        self._parent_id = parent_id
        
        # y, x
        self.HEXAGON_INDICES_EVEN = [(0,-1), (1,0), (0,1),
                                    (-1,1), (-1,0), (-1,-1)]
        self.HEXAGON_INDICES_ODD = [(1,-1), (1,0), (1,1),
                                    (0,1), (-1,0), (0,-1)]
        self._dead = False
        self._finished = False


    def __contains__(self, index : Tuple[int]):
        for cell in self._cell_path:
            if cell.mat_index == index:
                return True
        return False
    

    def __str__(self):
        output = ''
        last_count = min(len(self._cell_path), 3)
        for cell in self._cell_path[-last_count:]:
            output += str(cell.mat_index)
            output += ' '
        return output
    

    def __repr__(self) : return self.__str__()


    @property
    def last(self):
        return self._cell_path[-1]

    @property
    def cell_path(self):
        return self._cell_path

    @property
    def dead(self):
        return self._dead
    

    def die(self):
        self._dead = True
    

    @property
    def finished(self):
        return self._finished


    def finish(self):
        self._finished = True


    @property
    def obstacles(self) -> List["SFAGPath"]:
        return self._obstacles

    
    @property
    def id(self):
        return self._id
    
    @id.setter
    def id(self, value):
        self._id = value


    @property
    def parent_id(self):
        return self._parent_id

    
    @property
    def neighbors(self):
        return self._neighbors


    def add_point(self, point : Point):
        self._points.append(point)
    

    def step(self, section : List[Cell],
            paths : List["SFAGPath"],
            ended_paths : List["SFAGPath"]) -> List["SFAGPath"]:
        hexagon_indices = (self.HEXAGON_INDICES_EVEN if self.last.mat_index[1] % 2 == 0
                           else self.HEXAGON_INDICES_ODD)
        possible_steps = [hexagon_indices[(self._direction + i) % 6] for i in range(-1, 2)]
        

        steps : List = []
        obstacles : List = []
        for i, step in enumerate(possible_steps):
            next_index = (self.last.mat_index[0] + step[0], self.last.mat_index[1] + step[1])
            # Здесь только возможные шаги, теоретически возможные
            skip = True
            for cell in section:
                if cell.mat_index == next_index:
                    skip = False
                    break
            if skip:
                continue
            for path in paths:
                if next_index in path:
                    obstacles.append(path)
                    skip = True
                    break
            if skip:
                continue
            for ended_path in ended_paths:
                if ended_path in path:
                    obstacles.append(ended_path)
                    skip = True
                    break
            if skip:
                continue
            
            next_direction = self._direction + i - 1
            for cell in section:
                if cell.mat_index == next_index:
                    next_cell = cell
                    break
            steps.append(SFAGPath(i, self._cell_path + [next_cell],
                next_direction, self._id))

        if len(steps) == 0:
            self._obstacles = obstacles
            # neighbors
            self._neighbors = []
            neighbor_steps = [hexagon_indices[(self._direction + i) % 6] for i in [-2, 2]]
            for step in neighbor_steps:
                next_index = (self.last.mat_index[0] + step[0], self.last.mat_index[1] + step[1])
                for path in paths:
                    if next_index in path:
                        self._neighbors.append(path)
                        skip = True
                        break
                if skip:
                    continue
                for ended_path in ended_paths:
                    if ended_path in path:
                        self._neighbors.append(ended_path)
                        skip = True
                        break
                if skip:
                    continue

        return steps

    
    def adjust_fork(self, other : "SFAGPath"):
        for i, cell in reversed(list(enumerate(other.cell_path))):
            hexagon_indices = (self.HEXAGON_INDICES_EVEN if cell.mat_index[1] % 2 == 0
                           else self.HEXAGON_INDICES_ODD)
            neighboorhood = [(cell.mat_index[0] + step[0], cell.mat_index[1] + step[1])
                for step in hexagon_indices]
            adjacent_indices = [index for index in neighboorhood if index in self]
            if len(adjacent_indices) > 0:
                break
        branch_origin = i
        for i, cell in enumerate(self.cell_path):
            if cell.mat_index in adjacent_indices:
                fork_origin_index = i + 1
                break
        line = self._cell_path[:fork_origin_index]
        fork1 = self._cell_path[fork_origin_index:]
        fork2 = other.cell_path[branch_origin:]
        fork1len = len(self._cell_path) - fork_origin_index - 1
        fork2len = len(other.cell_path) - branch_origin - 1
        return line, fork1, fork2, fork1len, fork2len


    
