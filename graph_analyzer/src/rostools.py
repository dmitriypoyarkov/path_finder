import numpy as np
import shapely.geometry as sh
from geometry_msgs.msg import Point, Pose, Quaternion
from map_msgs.msg import OccupancyGridUpdate
from nav_msgs.msg import OccupancyGrid
from tf.transformations import quaternion_inverse

import my_math


def index_to_world_lst(self, r : int, c : int):
    y = self.info.origin.position.y + r * self.info.resolution
    x = self.info.origin.position.x + c * self.info.resolution

    
    # point_relative = Point(x=y_relative, y=x_relative)
    # q = self.info.origin.orientation
    # inv = quaternion_inverse([q.x, q.y, q.z, q.w])
    # q = Quaternion(x=inv[0], y=inv[1], z=inv[2], w=inv[3])
    # point = my_math.rotate_2d_point_gmsgs(point_relative, q)
    
    # y_relative = self.info.origin.position.y + c * self.info.resolution
    # x_relative = self.info.origin.position.x + r * self.info.resolution
    return [x, y]


def index_to_world(self, r : int, c : int):
    y_relative = self.info.origin.position.y + c * self.info.resolution
    x_relative = self.info.origin.position.x + r * self.info.resolution
    
    point_relative = Point(x=y_relative, y=x_relative)
    point = my_math.rotate_2d_point_gmsgs(point_relative,
        self.info.origin.orientation)
    
    return point


def index_to_world_lst1(self, r : int, c : int):
    y_relative = self.info.origin.position.y + c * self.info.resolution
    x_relative = self.info.origin.position.x + r * self.info.resolution
    
    point_relative = Point(x=y_relative, y=x_relative)
    q = self.info.origin.orientation
    inv = quaternion_inverse([q.x, q.y, q.z, q.w])
    q = Quaternion(x=inv[0], y=inv[1], z=inv[2], w=inv[3])
    point = my_math.rotate_2d_point_gmsgs(point_relative, q)
    
    return [point.x, point.y]


OccupancyGrid.index_to_world = index_to_world
OccupancyGrid.index_to_world_lst = index_to_world_lst

OccupancyGridUpdate.index_to_world_lst = index_to_world_lst1

def subtract_point(self : Point, other : Point):
    return Point(x = self.x - other.x,
                 y = self.y - other.y,
                 z = self.z - other.z)


def point_magnitude2D(self : Point):
    return (self.x**2 + self.y**2)**0.5


def point_multiply(self : Point, value : float):
    return Point(x = self.x * value,
                 y = self.y * value,
                 z = self.z * value)


def point_normalize2D(self : Point):
    magnitude = self.magnitude2D()
    if magnitude == 0:
        return
    self.x /= magnitude
    self.y /= magnitude
    return self


def point_negative(self : Point):
    return Point(
        x=-self.x,
        y=-self.y,
        z=-self.z
    )


def add_point(self : Point, other : Point):
    return Point(
        x=self.x + other.x,
        y=self.y + other.y,
        z=self.z + other.z
    )


Point.__neg__ = point_negative
Point.__sub__ = subtract_point
Point.__add__ = add_point
Point.__mul__ = point_multiply
Point.magnitude2D = point_magnitude2D
Point.normalize2D = point_normalize2D


def vector_direction2D(vec : Point) -> float:
    if vec.x == 0 and vec.y == 0:
        return 0
    return np.arctan2(vec.y, vec.x)


Point.direction2D = vector_direction2D


def vector_circle_intersection(vector : Point, origin : Point,
        center : Point, radius : float):
    p = sh.Point(center.x, center.y)
    c = p.buffer(radius).boundary
    vector.normalize2D()
    vector *= 2 * radius
    l = sh.LineString([(center.x, 0), (vector.x, vector.y)])
    i = c.intersection(l)
    if not isinstance(i, sh.Point):
        raise Exception('Multiple intersections')
    return Point(x=i.x, y=i.y)


def quaternion_neg(self : Quaternion):
    q = my_math.quaternion_inverse(
        [self.x,
        self.y, self.z, self.w]
    )
    return Quaternion(
        x=q[0], y=q[1], z=q[2], w=q[3]
    )


Quaternion.__neg__ = quaternion_neg