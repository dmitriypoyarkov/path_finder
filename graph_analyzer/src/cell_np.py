from time import time
from turtle import distance
from typing import List, Tuple, Union

import numpy as np
from geometry_msgs.msg import Point
from numba import float32, int16, int32  # import the types
from numba.experimental import jitclass

import my_math

spec = [
    ('_center', float32[:]),
    ('_height', float32),
    ('_radius', float32),
    ('_outer_radius_sqr', float32),
    ('_inner_radius_sqr', float32),
    ('_area', float32),
    ('_mat_index', int16[:]),
    ('_points', float32[:, :]),
    ('_source_indices', int16[:])
    
]

@jitclass(spec)
class CellNP(object):
    def __init__(self, center : np.ndarray, height : float, source_points : np.ndarray,
            mat_index : np.ndarray):
        self._center = center
        self._height = height
        self._radius = np.array(height*0.57735026919, np.float32) # 0.57... = 1/sqrt(3)

        self._outer_radius_sqr = self._radius**2
        self._inner_radius_sqr = (self._height * 0.5)**2
        self._area = 2.59807621 * self._outer_radius_sqr
        self._mat_index = mat_index

        self.select_points(source_points)


    def __repr__(self):
        return f'({self.center[0]:.2f}, {self.center[0]:.2f})'


    @property
    def mat_index(self):
        return self._mat_index


    @property
    def height(self):
        return self._height

    @property
    def points(self):
        return self._points

    @property
    def center(self):
        return self._center
    
    @property
    def area(self):
        return self._area

    @property
    def points_count(self):
        return self._points.shape[0]


    def contains(self, point : np.ndarray) -> bool:
        distance_sqr = my_math.sqr_distance_2D_np(self._center, point)
        if distance_sqr > self._outer_radius_sqr:
            return False
        if distance_sqr < self._inner_radius_sqr:
            return True

        angle0 = my_math.angle_between_vectors2D_np(
            my_math.VECTOR_30, point)
        angle0 = angle0 % my_math.PI_3
        angle0 = angle0 - my_math.PI_6
    
        if self._inner_radius_sqr / np.cos(angle0)**2 >= distance_sqr:
            return True
        return False
    

    # add points from list containing in this cell area
    def select_points(self, points : np.ndarray):
        self._points = np.zeros(points.shape, np.float32)
        self._source_indices = np.zeros(points.shape[0], np.int16)
        k = 0
        for i in range(points.shape[0]):
            if self.contains(points[i]):
                self._points[k] = points[i]
                self._source_indices[k] = i
                k += 1
        self._points = self._points[:k]
        self._source_indices = self._source_indices[:k]
    

    def remove_from_source(self, points: np.ndarray) -> np.ndarray:
        return np.delete(points, self._source_indices, axis=0)

def main():
    test = CellNP(
        np.array([0, 0], np.float32),
        2.0,
        np.array([
            [0.0, 1.00]
        ], np.float32),
        np.array([0, 0], np.int16)
    )
    print(test._source_indices)


if __name__ == "__main__":
    main()

