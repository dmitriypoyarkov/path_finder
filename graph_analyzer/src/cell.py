import math
from typing import List, Tuple, Union

from geometry_msgs.msg import Point

import my_math
from rect import Rect


class Cell(object):
    def __init__(self, center : Point, height : float, source_points : List[Point],
            mat_index : Union[Tuple[int], None] = None):
        self._center = center
        self._average_point = center
        self._shifted_point = center
        self._height = height
        self._radius = height/(math.sqrt(3))
        
        self._base_rect = Rect(
            Point(x=center.x + height/2,
                    y=center.y + self._radius),
            Point(x=center.x - height/2,
                    y=center.y - self._radius),
        )
        self._outer_radius_sqr = self._radius**2
        self._inner_radius_sqr = (height / 2)**2
        self._area = 2.59807621 * self._outer_radius_sqr
        self._mat_index = mat_index
        self._source_point_indices = self.select_points(source_points)


    def __repr__(self):
        return f'({self.center.x:.2f}, {self.center.y:.2f})'


    @property
    def mat_index(self):
        return self._mat_index

    @property
    def shifted_point(self):
        return self._shifted_point

    @property
    def height(self):
        return self._height

    @property
    def points(self):
        return self._points

    @property
    def center(self):
        return self._center
    
    
    @property
    def area(self):
        return self._area

    @property
    def points_count(self):
        return len(self._points)


    def contains(self, point : Point) -> bool:
        distance_sqr = my_math.sqr_distance(self._center, point)
        if distance_sqr > self._outer_radius_sqr:
            return False
        if distance_sqr < self._inner_radius_sqr:
            return True
        if self._base_rect.contains(point):
            return True
        rotated_60 = my_math.rotate_point(self._center, point, math.pi/3)
        if self._base_rect.contains(rotated_60):
            return True
        rotated_120 = my_math.rotate_point(self._center, point, 2 * math.pi/3)
        if self._base_rect.contains(rotated_120):
            return True
        return False
    

    # add points from list containing in this cell area
    def select_points(self, points : List[Point]):
        self._points = []
        source_point_indices = []
        sum = Point()
        for i, point in enumerate(points):
            if self.contains(point):
                sum = my_math.add(sum, point)
                self._points.append(point)
                source_point_indices.append(i)
        if len(self._points) == 0:
            self._average_point = self._center
            return []
        self._average_point = my_math.divide(sum, len(self._points))
        self._shifted_point = self._average_point
        return source_point_indices
    

    def shift_avg_point(self, shift_vector : Point):
        self._shifted_point = my_math.add(self._shifted_point, shift_vector)

    
    def density(self):
        return len(self._points) / self._radius
    

    def hexagon_shift(self):
        return self._height * (math.sqrt(3)/2)


    def clear(self):
        self._points.clear()
        self._average_point = self._center
        self._shifted_point = self._average_point

    
    def average_with(self, other_cell : "Cell") -> Point:
        all_points = self._points + other_cell.points
        return my_math.average2D(all_points)

    
    def remove_from_source(self, source : List[Point]) -> List[Point]:
        return [point for i, point in enumerate(source)
            if i not in self._source_point_indices]
