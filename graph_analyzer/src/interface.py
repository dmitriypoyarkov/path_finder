import os
import socket
import sys
from signal import SIGINT, signal
from subprocess import PIPE, CalledProcessError, Popen, call, check_output, run
from threading import Thread
from time import sleep, time

import pexpect
from net_control import NetControl
import rospy
from actionlib import GoalStatus
from cv_bridge import CvBridge
from paramiko.ssh_exception import AuthenticationException
from pexpect import EOF, TIMEOUT, ExceptionPexpect, pxssh, spawn
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import (QCoreApplication, QObject, QSettings,
                          QStringListModel, QThread, QTimer, pyqtSignal)
from PyQt5.QtGui import QIcon, QImage, QPixmap
from PyQt5.QtWidgets import QAction, QCompleter, QMainWindow, QMessageBox
from PyQt5.QtTest import QTest
from rospkg import ResourceNotFound, RosPack
from std_msgs.msg import UInt8

from base_interface import Ui_MainWindow
from pf_client1 import PathFinderClient


class Worker(QObject):
    finished = pyqtSignal()
    progress = pyqtSignal(int)

    def __init__(self, action : callable):
        super().__init__()
        self.action = action
        self.control = {'stop' : False}


    def run_listen(self):
        """Long-running task."""
        self.timer = QTimer(self)
        while True:
            self.action()
    

    def stop(self):
        self.timer.stop()
        self.finished.emit()

    
    def run(self):
        self.action()
        self.finished.emit()


class Ui_MainWindowFunctional(Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.cvBridge = CvBridge()
        
        self.afterUnlockSetup = None
        self.roslaunchProcess = None
        self.sshClient = None
        self.sshClient1 = None
        self.sshStdin = None
        self.roscoreProcess = None
        self.systemOnline = False
        self.outputWorker = None
        self.outputThread = None
        self.thread = None
        self.worker = None


    def setupUi(self, MainWindow : QMainWindow):
        self.mainWindow = MainWindow
        return super().setupUi(MainWindow)


    def setupFunctional(self):
        self.mainWindow.setWindowTitle('Path Finder Manager')
        filepath = os.path.dirname(os.path.abspath(__file__))
        icon_path = os.path.expanduser(f'{filepath}/../img/logo.png')
        self.mainWindow.setWindowIcon(QIcon(icon_path))

        # disable buttons by default
        self.initButtonSetup()
        self.startRoscore()

        self._pf_client = PathFinderClient()
        self._pf_client.set_done_cb(self.doneCallback)
        self._net_control = NetControl(self._pf_client,
            net=[[1], [3, 2, 0], [1], [4, 5, 1], [3], [7, 6, 3], [5], [5]],
            cur_position=0
        )
        self.initNetControlPanel(self._net_control.vertex_num)

        self.actionRun.triggered.connect(self._pf_client.send_start_command)

        self.startButton.pressed.connect(self.onStartButtonPressed)

        self.stopButton.pressed.connect(self.onStopButtonPressed)

        self.launchButton.pressed.connect(self.onLaunchButtonPressed)

        self.shutdownButton.pressed.connect(lambda : print('Shutdown button pressed'))
        self.shutdownButton.pressed.connect(self.onShutdownButtonPressed)

        self.relaunchButton.pressed.connect(self.onRelaunchButtonPressed)

        self.launchModeDropdown.currentIndexChanged.connect(
            self.onLaunchModeChanged
        )

        self.sshConnectButton.pressed.connect(
            self.onSshConnectButtonPressed
        )

        self._pf_client.set_frame_callback(self.frameCallback)

        self._pf_client.add_fork_callback(self.forkCallback)

        self.forkNum = None
        self.forkReceived = False
        self.forkEventTimer = QTimer()
        self.forkEventTimer.start(100)
        self.forkEventTimer.timeout.connect(self.forkEventCheck)

        self.turnLeftButton.pressed.connect(lambda : self.onTurnButton(1))
        self.turnRightButton.pressed.connect(lambda : self.onTurnButton(0))

        self.mainWindow.closeEvent = self.closeEvent

        self.setupCompleters()

        self.packageInput.textChanged.connect(self.onPackageTextChanged)
        self.onPackageTextChanged()

        self.openLaunchFileButton.pressed.connect(self.onOpenLaunchFilePressed)

        self.controlModeDropdown.currentIndexChanged.connect(
            self.stackedControlPanel.setCurrentIndex
        )

        self.positionDropdown.activated.connect(self.onPositionDropdownActivated)

        self.setupSubscribers()

        self.loadSettings()
        # quit = QAction("Quit", self.mainWindow)
        # quit.triggered.connect(self.closeEvent)


    def setupSubscribers(self):
        self._subs = [
            rospy.Subscriber('~cur_position', UInt8, self.refreshNetPosition)
        ]
    

    def destroySubscribers(self):
        for sub in self._subs:
            sub.unregister()


    def onPositionDropdownActivated(self, index : int):
        self._net_control.cur_position = index


    def refreshNetPosition(self, msg : UInt8):
        self.positionDropdown.setCurrentIndex(msg.data)


    def doneCallback(self, state, result):
        print('DEBUG doneCallback Invoked')
        self.startButton.setEnabled(True)
        self.stopButton.setEnabled(False)
        self.controlModeDropdown.setEnabled(True)
        self.netControlPanel.setEnabled(True)


    def initNetControlPanel(self, vertex_num : int):
        for i in range(vertex_num):
            self.goalDropdown.addItem(str(i))
            self.positionDropdown.addItem(str(i))


    def checkCommandOutput(self, command : str):
        if self.launchModeDropdown.currentText() == "SSH":
            # TODO: fix
            self.sshClient.sendline(command)
            self.sshClient.prompt()
            output = self.sshClient.before
            return output
        if self.launchModeDropdown.currentText() == "Local":
            return str(check_output(command.split()), 'utf-8')


    def setupCompleters(self):
        packages_lines = self.checkCommandOutput("rospack list").splitlines()
        packages = [line.split()[0] for line in packages_lines]
        model = QStringListModel()
        model.setStringList(packages)

        completer = QCompleter()
        completer.setModel(model)

        self.packageInput.setCompleter(completer)
        self.onPackageTextChanged()


    def onLaunchModeChanged(self, index):
        self.connectionStackedWidget.setCurrentIndex(index)

        if index == 1: # SSH
            self.connectedInterfaceState(False)
        if index == 0:
            self.connectedInterfaceState(True)


    def onSshConnectButtonPressed(self):
        try:
            self.connectThroughSsh()
        except (pxssh.ExceptionPxssh, OSError, pexpect.exceptions.EOF) as e:
            self.showErrorMessage('SSH Login error')
            print(e)
            return
        # success
        self.connectedInterfaceState(True, switchTab=False)
    

    def connectedInterfaceState(self, connected : bool, switchTab=False):
        self.packageInput.setEnabled(connected)
        self.launchFileInput.setEnabled(connected)
        self.controlTab.setEnabled(connected)
        self.openLaunchFileButton.setEnabled(connected)

        self.packageLabel.setEnabled(connected)
        self.launchFileLabel.setEnabled(connected)

        if switchTab:
            self.tabWidget.setCurrentIndex(0)


    def launchThroughSsh(self):
        self.sshClient.sendline(self.getTravelerLaunchCommand) # run a command

        self.continuous_output_thread()


    def continuous_output_thread(self):
        if self.outputWorker:
            return
        self.outputThread = QThread()
        self.outputWorker = Worker(self.continuous_output)
        # self.shutdownSygnal.connect(self.outputWorker.stop)
        self.outputWorker.moveToThread(self.outputThread)
        self.outputThread.started.connect(self.outputWorker.run_listen)
        self.outputWorker.finished.connect(self.outputThread.quit)
        self.outputWorker.finished.connect(self.outputWorker.deleteLater)
        self.outputThread.finished.connect(self.outputThread.deleteLater)
        self.outputThread.start()
    

    def continuous_output(self):
        # print('??')
        try:
            line = self.sshClient.readline()
            if len(line) > 0:
                print(line, end='')
        except TIMEOUT:
            pass
        except OSError:
            return
        except ValueError:
            return
        except ExceptionPexpect:
            return


    def onPackageTextChanged(self):
        model = QStringListModel()
        try:
            packagePath = self.getPackagePath(self.packageInput.text())
        except ResourceNotFound:
            return
        launchPath = os.path.join(packagePath, 'launch')
        launches = [file for file in os.listdir(launchPath) if file.endswith('.launch')]
        model.setStringList(launches)

        completer = QCompleter()
        completer.setModel(model)

        self.launchFileInput.setCompleter(completer)


    def getPackagePath(self, packageName : str):
        packages_lines = self.checkCommandOutput('rospack list').splitlines()
        package_dict = {
            line.split()[0]:
            line.split()[1]
            for line in packages_lines
        }
        try:
            path = package_dict[packageName]
            return path
        except KeyError:
            raise ResourceNotFound('Package not found:', packageName)


    def onOpenLaunchFilePressed(self):
        try:
            pathInput = self.packageInput.text()
            packagePath = self.getPackagePath(pathInput)
        except:
            self.showErrorMessage(f'No package found: {pathInput}')
            return
        launchName = self.launchFileInput.text()
        launchPath = os.path.join(packagePath, f'launch/{launchName}')
        if not os.path.exists(launchPath) or not os.path.isfile(launchPath):
            self.showErrorMessage(f'No launch file found: {launchPath}')
            return
        call(['xdg-open', launchPath])
    

    def showErrorMessage(self, message : str):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Warning)
        msg.setText(message)
        msg.setWindowTitle('Error')
        msg.exec_()


    def forkCallback(self, fork_num : UInt8):
        if self.controlModeDropdown.currentText == 'Net':
            return
        self.forkNum = fork_num.data
        self.forkReceived = True



    def forkEventCheck(self):
        if self.forkReceived:
            self.actionStatusLabel.setText('Turn choice expecting')
            self.enableTurnButtons()
            self.forkReceived = False


    def enableTurnButtons(self, value=True):
        self.manualControlPanel.setEnabled(value)


    def onTurnButton(self, turn : int):
        self._pf_client.send_turn(turn)
        self.enableTurnButtons(False)
        self.forkNum = None


    def onStartButtonPressed(self):
        self.startButton.setEnabled(False)
        self.netControlPanel.setEnabled(False)
        self.manualControlPanel.setEnabled(False)
        self.controlModeDropdown.setEnabled(False)

        print('DEBUG Start pressed' )
        if self.controlModeDropdown.currentText() == 'Manual':
            self._pf_client.send_start_command()

        if self.controlModeDropdown.currentText() == 'Net':
            self._net_control.cur_position = int(self.positionDropdown.currentText())
            self._net_control.init_movement(
                int(self.goalDropdown.currentText())
            )

        print('DEBUG Move base waiting')

        self.waitForMoveBaseActive()
        self.actionStatusLabel.setText('Moving')
        self.stopButton.setEnabled(True)


    def waitForMoveBaseActive(self):
        state = self._pf_client._action_client.get_state()
        while state != GoalStatus.ACTIVE:
            QTest.qSleep(100)
            state = self._pf_client._action_client.get_state()


    def onStopButtonPressed(self):
        self.stopButton.setEnabled(False)
        self._pf_client.send_stop_command()
        state = self._pf_client._action_client.get_state()
        while (state not in [GoalStatus.PREEMPTED,
                GoalStatus.RECALLED, GoalStatus.SUCCEEDED]):
            sleep(0.1)
            state = self._pf_client._action_client.get_state()
            print(state)
        self.actionStatusLabel.setText('Stopped')
        self.startButton.setEnabled(True)


    def closeEvent(self, event):
        print('hm...')
        self.shutdownSystem()
        self.sshDisconnect()
        self.shutdownRoscore()
        self.saveSettings()
        event.accept()


    def sshDisconnect(self):
        if self.sshClient:
            self.sshClient.close()


    def startRoscore(self):
        if not self.roscoreOnline():
            bashCommand = 'roscore'
            self.roscoreProcess = Popen(bashCommand.split())
    

    def shutdownRoscore(self):
        if not self.roscoreProcess:
            return
        self.roscoreProcess.send_signal(SIGINT)
        self.roscoreProcess.wait()


    def launchSystem(self):
        self.launchButton.setEnabled(False)
        self.systemStack.setCurrentIndex(2)

        print('DEBUG', self.outputThread, self.outputWorker)
        if self.launchModeDropdown.currentText() == 'Local':
            self.launchLocally()
        if self.launchModeDropdown.currentText() == 'SSH':
            self.launchThroughSsh()

        self.startInThread(self._pf_client.wait_for_server,
            self.onSystemLaunch)
        self.shutdownButton.setEnabled(True)
        self.relaunchButton.setEnabled(True)

        self.actionStatusLabel.setText('None')
        # self._pf_client.wait_for_server()
        # self.process = run(bashCommand.split(), capture_output=True)

        # self.process = QtCore.QProcess(self._mainWindow)
        # self.process.start(
        #     'roslaunch',
        #     ['yarp8_config', 'yarp8_gazebo_empty.launch'],
        # )
        # print('ok')
        # self.process.readyRead.connect(self.dataReady)
        # Stream(newText=self.dataReady)
        # QProcess emits `readyRead` when there is data to be read


    def onSystemLaunch(self):
        print('DEBUG onSystemLaunch')
        self.systemStack.setCurrentIndex(0)
        self.stackedControlPanel.setEnabled(True)
        self.manualControlPanel.setEnabled(False)
        self.netControlPanel.setEnabled(True)

        self.launchedButtonsSetup()


    def roscoreOnline(self):
        try:
            topics : str = check_output('rostopic list'.split()).decode('utf-8')
        except CalledProcessError as e:
            if e.returncode == 1:
                return False
            else:
                rospy.logerr('Unknown error while checking roscore')
        if '/rosout\n' in topics:
            return True
        return False


    def startInThread(self, func : callable, finished_cb : callable,
            buttonSetup : callable = None):
        if buttonSetup:
            self.blockAllButtons(buttonSetup)
        self.thread = QThread()
        self.worker = Worker(func)
        self.worker.moveToThread(self.thread)
        self.thread.started.connect(self.worker.run)
        self.worker.finished.connect(finished_cb)
        self.worker.finished.connect(self.thread.quit)
        self.worker.finished.connect(self.worker.deleteLater)
        self.worker.finished.connect(self.releaseButtons)
        self.thread.finished.connect(self.thread.deleteLater)
        self.thread.start()


    def blockAllButtons(self, afterUnlockSetup : callable):
        self.afterUnlockSetup = afterUnlockSetup
        self.launchButton.setEnabled(False)
        self.shutdownButton.setEnabled(False)
        self.relaunchButton.setEnabled(False)
        self.startButton.setEnabled(False)
        self.stopButton.setEnabled(False)
        self.manualControlPanel.setEnabled(False)
        self.netControlPanel.setEnabled(False)

        self.configureTab.setEnabled(False)


    def releaseButtons(self):
        print('Releasing')
        if not self.afterUnlockSetup:
            return
        self.afterUnlockSetup()
        self.afterUnlockSetup = None


    def getTravelerLaunchCommand(self):
        launch_path = os.path.join(
            self.getPackagePath(self.packageInput.text()),
            self.launchFileInput.text()
        )
        return ' '.join([
            'graph_analyzer visual_path_traveler.launch',
            f'robot_launch_path:={launch_path}'
            f'image_raw_topic:={self.imageRawInput}',
            f'camera_info_topic:={self.cameraInfoInput}',
            f'move_base_topic:={self.moveBaseInput}',
            f'base_link_frame:={self.baseLinkInput}'
        ])
    

    def getRobotLaunchCommand(self):
        return ' '.join([
            self.packageInput.text(),
            self.launchFileInput.text()
        ])


    def launchLocally(self):
        self.roslaunchProcess = Popen(
            self.getTravelerLaunchCommand().split()
        )
        self.travellerProcess = Popen(

        )


    def connectThroughSsh(self):
        hostname = self.hostnameLineInput.text()
        port = self.portLineInput.text()
        username = self.userLineInput.text()
        password = self.passwordLineInput.text()
    
        self.sshClient = pxssh.pxssh(encoding='utf-8')
        self.sshClient.login(hostname, username, password)
        # self.sshClient.logfile = sys.stdout
        # self.sshClient.stdout = sys.stdout
        



        # self.sshClient = paramiko.client.SSHClient()
        # self.sshClient.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        # self.sshClient.connect(host, port=port, username=username, password=password)

        # self.sshClient1 = paramiko.client.SSHClient()
        # self.sshClient1.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        # self.sshClient1.connect(host, username=username, password=password)



        # # key = paramiko.RSAKey.from_private_key_file(os.path.expanduser('~/.ssh/id_rsa.pem'))

        # self.sshClient = paramiko.SSHClient()

        # self.sshClient.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        # self.sshClient.connect(
        #     hostname=host,
        #     port=port,
        #     username=username,
        #     pkey=key
        # )
        # self.sshStdin, stdout, stderr = self.sshClient.exec_command(command)


    def shutdownSystem(self):
        if self.launchModeDropdown.currentText() == 'Local':
            if self.roslaunchProcess:
                self.roslaunchProcess.send_signal(SIGINT)
                self.roslaunchProcess.wait()
        if self.launchModeDropdown.currentText() == 'SSH':
            if self.sshClient and self.sshClient:
                # self.sshClient.kill(SIGINT)
                try:
                    self.sshClient.send(chr(3))
                except OSError:
                    pass
                # self.sshClient.close()
        self.systemStack.setCurrentIndex(1)


    def onShutdownButtonPressed(self):
        print('DEBUG shutdown pressed')
        self.destroySubscribers()
        print('DEBUG Subs destroyed')
        self.shutdownSystem()
        print('DEBUG system shutdown')
        self.stopThreads()
        print('DEBUG threads stopped')
        self.initButtonSetup()
        print('DEBUG buttons restored')


    def stopThreads(self):
        try:
            print('DEBUG shutting down worker')
            if self.worker:
                self.worker.deleteLater()
                self.thread.wait()
        except RuntimeError:
            pass
        print('DEBUG worker ok')
        # try:
        #     print('DEBUG shutting down output')
        #     if self.outputWorker:
        #         self.outputWorker.stop()
        #         self.outputThread.wait()
        # except RuntimeError:
        #     pass
        # print('DEBUG output ok')


    def onLaunchButtonPressed(self):
        self.launchSystem()


    def onRelaunchButtonPressed(self):
        self.onShutdownButtonPressed()
        self.onLaunchButtonPressed()


    def launchedButtonsSetup(self):
        self.launchButton.setEnabled(False)
        self.shutdownButton.setEnabled(True)
        self.relaunchButton.setEnabled(True)
        self.startButton.setEnabled(True)

        self.configureTab.setEnabled(False)
        self.controlModeDropdown.setEnabled(True)


    def initButtonSetup(self):
        self.launchButton.setEnabled(True)
        self.shutdownButton.setEnabled(False)
        self.startButton.setEnabled(False)
        self.stopButton.setEnabled(False)
        self.stackedControlPanel.setEnabled(False)
        self.relaunchButton.setEnabled(False)

        self.configureTab.setEnabled(True)
        self.controlTab.setEnabled(True)
    

    def frameCallback(self, data):
        img = self.cvBridge.imgmsg_to_cv2(data, 'rgb8')
        QPixmap()
        height, width, channel = img.shape
        bytesPerLine = 3 * width
        qImg = QImage(img.data, width, height, bytesPerLine, QImage.Format_RGB888)
        self.frameLabel.setPixmap(QPixmap(qImg))

    
    def saveSettings(self):
        settings = QSettings('FC', 'Path Finder Manager')
        settings.setValue('configureTab/package', self.packageInput.text())
        settings.setValue('configureTab/launchFile', self.launchFileInput.text())
        settings.setValue('configureTab/launchMode', self.launchModeDropdown.currentIndex())
        settings.setValue('configureTab/user', self.userLineInput.text())
        settings.setValue('configureTab/hostname', self.hostnameLineInput.text())
        settings.setValue('configureTab/port', self.portLineInput.text())
        settings.setValue('configureTab/password', self.passwordLineInput.text())
        settings.setValue('controlTab/controlMode', self.controlModeDropdown.currentIndex())
    

    def loadSettings(self):
        settings = QSettings('FC', 'Path Finder Manager')
        self.packageInput.setText(settings.value('configureTab/package', ''))
        self.launchFileInput.setText(settings.value('configureTab/launchFile', ''))
        launchModeIndex = int(settings.value('configureTab/launchMode', 0))
        self.launchModeDropdown.setCurrentIndex(launchModeIndex)
        self.onLaunchModeChanged(launchModeIndex)
        self.userLineInput.setText(settings.value('configureTab/user', ''))
        self.hostnameLineInput.setText(settings.value('configureTab/hostname', ''))
        self.portLineInput.setText(settings.value('configureTab/port', '22'))
        self.passwordLineInput.setText(settings.value('configureTab/password', ''))
        self.controlModeDropdown.setCurrentIndex(
            int(settings.value('controlTab/controlMode', 0))
        )
        self.stackedControlPanel.setCurrentIndex(self.controlModeDropdown.currentIndex())


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    signal(SIGINT, lambda _0, _1 : app.closeAllWindows())

    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindowFunctional()
    ui.setupUi(MainWindow)
    ui.setupFunctional()
    MainWindow.show()
    timer = QTimer()
    timer.start(500)
    timer.timeout.connect(lambda: app.processEvents())
    sys.exit(app.exec_())