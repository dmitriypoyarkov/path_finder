from typing import Union, overload
from geometry_msgs.msg import Point
import my_math

import rostools

class Ray(object):
    @overload
    def __init__(self, p1 : Point, p2 : Point) -> "Ray": ...
    @overload
    def __init__(self, origin : Point = Point(), direction : float = 0.0) -> "Ray": ...

    def __init__(self, origin : Point = Point(),
            direction : Union[float, Point] = 0.0):
        self._origin = origin
        if isinstance(direction, Point):
            direction = my_math.vector_angle2D(direction - origin)
        
        self._origin = origin
        self._direction = direction
    

    @property
    def direction(self):
        return self._direction

    
    @property
    def origin(self):
        return self._origin