#!/usr/bin/env python3
import rospy
from led_strip_control.srv import LedStripCmdRequest, LedStripCmdResponse, LedStripCmd
from graph_analyzer.msg import PathFinderState

state_topic = '/global_planner_service_node/path_finder_state'
led_service = '/yarp8r2/control/led_server_1/set_animation'

class YarpLightControl(object):
    def __init__(self, state_topic : str, led_service : str):
        rospy.init_node('yarp8_lights')
        self._cur_state = None


        rospy.loginfo('Waiting for led service...')
        rospy.wait_for_service(led_service)
        self._led_client = rospy.ServiceProxy(led_service, LedStripCmd)
        
        self._animation_map = {
            PathFinderState.IDLE : 'idle.json',
            PathFinderState.START : 'idle.json',
            PathFinderState.FINISH : 'idle.json',
            PathFinderState.PATH_FOLLOW : 'move.json',
            PathFinderState.FORK_CHOICE_WAIT : 'fork_wait.json',
            PathFinderState.SEARCH : 'search.json',
            PathFinderState.DIRECTING : 'search.json'
        }

        rospy.Subscriber(state_topic, PathFinderState, self.state_callback)
        rospy.loginfo('Inited!')


    def str_to_param_array(self, req_str : str):
        return [ord(char) for char in req_str]


    def state_callback(self, msg : PathFinderState):
        if msg.state == self._cur_state:
            return
        self._cur_state = msg.state
        try:
            animation = self._animation_map[msg.state]
        except KeyError:
            if msg.state == PathFinderState.FORK_MOVE:
                if msg.turn_index <= ((msg.turns_total - 1) / 2):
                    animation = 'turn_right.json'
                else:
                    animation = 'turn_left.json'
            else:
                raise ValueError('Wrong state passed')
        
        colors = []
        params = self.str_to_param_array(animation)
        duration = 0
        set_default = False
        self._led_client(LedStripCmdRequest.CUSTOM,
                    colors,
                    params,
                    duration,
                    set_default)


if __name__ == '__main__':
    control = YarpLightControl(state_topic, led_service)
    rospy.spin()