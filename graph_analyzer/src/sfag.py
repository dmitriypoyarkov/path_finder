import copy
import itertools
import math
from typing import List, Tuple

import numpy as np
import rospy
from geometry_msgs.msg import Point

import my_math
from cell import Cell
from cell_np import CellNP
from fpath import FPath
from rect import Rect
from sfag_path import SFAGPath


class SFAG(object):
    def __init__(self, rect : Rect):
        # bounds
        self._top_left = rect.top_left
        self._bottom_right = rect.bottom_right
        self._rect = rect
        self._bigger_size = max(rect.height, rect.width)

        # x, y
        self._relative_index_order = [
            (0, 0),
            (-1, 0), (-1, 1), (0, 1), (1, 1), (1, 0)
            ]

        self.GRID_STEP = 0.2
        self.SPLIT_SCALES = self.init_split_scales(1, 40)
        self.POINT_NUM_THRESHOLD = 0.8
        self.MIN_SPLIT = 0.05
        self.SPLIT_STEPS = 8
        self.SPLIT_STEP = (self._bigger_size - self.MIN_SPLIT)/self.SPLIT_STEPS
        self.MAX_DISTRIBUTION = 11
        self.MIN_DISTRIBUTION = 1
        
        self.DENSITY_SPREAD = 0.33
        self.EQUALITY_COEFF = 0.20
        self.HEXAGON_INDICES_EVEN = [(0,-1), (1,0), (0,1),
                                    (-1,1), (-1,0), (-1,-1)]
        self.HEXAGON_INDICES_ODD = [(1,-1), (1,0), (1,1),
                                    (0,1), (-1,0), (0,-1)]
        self.MIN_FORK_ANGLE = 0.32175055 # radians
        self.MIN_FORK_LENGTH = 0.1
        self.FORK_POS_DISPERSION = 0.4
        self.SPLIT_AREA_LIMIT = 0.1


    def init_split_scales(self, min : int, max : int):
        power = 1.5
        min_powered = math.floor(min**(1/power))
        max_powered = math.floor(max**(1/power))
        return [math.floor(x**1.5) for x in range(min_powered, max_powered)]


    def filter_new(self, points : List[Point], debug : bool = False) -> FPath:
        # results of different distributions
        fpaths : List[FPath] = []
        all_fpaths : List[FPath] = []
        print('Filtering a set...')
        split = self.autosplit(points)
        path, fork_steps = self.build_graph(split['cells_mat'])
        fpath = FPath(path_indices=path, fork_steps=fork_steps, cells_mat=split['cells_mat'],
            distribution_scale=split['scale'], x_shift=split['x_shift'], y_shift=split['y_shift'])

        fpath.average_density = np.average([split['cells_mat'][index[0]][index[1]].density() for index in path + fork_steps])
        fpath.average_path_density = np.average([split['cells_mat'][index[0]][index[1]].density() for index in path])
        fpath.cells_mat = split['cells_mat']
        
        return fpath


    def distribute(self, distrib_scale : int, points : List[Point],
                    min_corner : Point, max_corner : Point,
                    x_shift : float, y_shift : float) -> List[List[Cell]]:
        points_copy = points
        cell_size = self._bigger_size / distrib_scale
        min_x = min_corner.x + (x_shift - 1) * cell_size
        min_y = min_corner.y + (y_shift - 2) * cell_size

        horizontal_shift = Cell(Point(), cell_size, []).hexagon_shift()
        horizontal_cell_number = math.ceil((max_corner.y - min_y) / horizontal_shift) + 1
        vertical_cell_number = math.ceil((max_corner.x - min_x) / cell_size) + 2
        
        ordered_indices = self.init_indices(horizontal_cell_number,
            vertical_cell_number)
        # cells_mat = [[None] * vertical_cell_number] * horizontal_cell_number
        cells_mat = [[None for _ in range(horizontal_cell_number)]
            for _ in range(vertical_cell_number)]
        for y, x in ordered_indices:
            cell = Cell(
                Point(x=min_x + x * cell_size + (0.5 * cell_size if y % 2 == 1 else 0),
                        y=min_y + y * horizontal_shift),
                cell_size,
                points_copy,
                mat_index=(x, y)
            )
            points_copy = cell.remove_from_source(points_copy)
            cells_mat[x][y] = cell
        return cells_mat

    
    def distribute_np(self, distrib_scale : int, points : np.ndarray,
                    min_corner : np.ndarray, max_corner : np.ndarray,
                    x_shift : float, y_shift : float) -> List[List[CellNP]]:
        points_copy = np.array(points)
        cell_size = self._bigger_size / distrib_scale
        min_x = min_corner.x + (x_shift - 1) * cell_size
        min_y = min_corner.y + (y_shift - 2) * cell_size

        horizontal_shift = Cell(Point(), cell_size, []).hexagon_shift()
        horizontal_cell_number = math.ceil((max_corner.y - min_y) / horizontal_shift) + 1
        vertical_cell_number = math.ceil((max_corner.x - min_x) / cell_size) + 2
        
        ordered_indices = self.init_indices(horizontal_cell_number,
            vertical_cell_number)
        # cells_mat = [[None] * vertical_cell_number] * horizontal_cell_number
        cells_mat = [[None for _ in range(horizontal_cell_number)]
            for _ in range(vertical_cell_number)]
        for y, x in ordered_indices:
            cell = CellNP(
                np.array([min_x + x * cell_size + (0.5 * cell_size if y % 2 == 1 else 0),
                    min_y + y * horizontal_shift], np.float32),
                np.array(cell_size, np.float32),
                points_copy,
                mat_index=np.array([x, y], np.int16)
            )
            points_copy = cell.remove_from_source(points_copy)
            cells_mat[x][y] = cell
        return cells_mat


    def min_area_split(self, scale : int, points : List[Point]):
        splits = []

        for x_shift in np.arange(0, 1, 0.4):
            for y_shift in np.arange(0, 1, 0.4):
                cells_mat = self.distribute(scale, points, x_shift, y_shift)
                nonempty_cells = [cell for row in cells_mat for cell in row if cell.density() > 0]
                area = sum([cell.area for cell in nonempty_cells])
                splits.append({'cells_mat' : cells_mat, 'scale' : scale,
                    'x_shift' : x_shift, 'y_shift' : y_shift, 'area' : area,
                    'cells' : nonempty_cells})
        target_split = min(splits, key=lambda split : split['area'])
        return target_split
    

    def autosplit(self, points : List[Point]):
        split0 = self.max_connected_section(points, self.GRID_STEP, self.SPLIT_SCALES[0])
        init_point_num = split0['point_num']
        threshold = math.floor(init_point_num * self.POINT_NUM_THRESHOLD)
        splits = [split0]
        for scale in self.SPLIT_SCALES:
            cur_split = self.max_connected_section(points, self.GRID_STEP, scale)
            if cur_split['point_num'] < threshold:
                try:
                    return splits[-2]
                except IndexError:
                    return splits[-1]
            splits.append(cur_split)
        rospy.logwarn('AUTOSPLIT: TOO SMALL SCALE DETECTED')
        return splits[-1]


    def connected_section(self, cells : List[List[Cell]]) -> List[Cell]:
        nonempty_cells = [(cell, [row.index(cell), cells.index(row)]) for row in cells for cell in row if cell.density() > 0]

        if len(nonempty_cells) == 0:
            return []
        
        # nearest to zero x
        cell0 = min(nonempty_cells, key=lambda cell : cell[0].center.x)
        equal_cells = [cell for cell in nonempty_cells if cell[0].center.x == cell0[0].center.x]
        cell0 = min(equal_cells, key=lambda cell : math.fabs(cell[0].center.y))

        connected_section = [cell0]
        remained_cells_to_process = [cell0]
        while len(remained_cells_to_process) > 0:
            cur_cell = remained_cells_to_process[0]
            if cur_cell[1][0] % 2 != 0:
                possible_neighbor_indices = [list(np.array(cur_cell[1]) + [x, y]) for x, y in itertools.product([-1, 0, 1], [0, 1])]
            else:
                possible_neighbor_indices = [list(np.array(cur_cell[1]) + [x, y]) for x, y in itertools.product([-1, 0, 1], [-1, 0])]
            neighbors = [cell for cell in nonempty_cells if cell[1] in possible_neighbor_indices and
                                                                        cell not in connected_section]
            connected_section.extend(neighbors)

            remained_cells_to_process.pop(0)
            remained_cells_to_process.extend(neighbors)
        
        return [item[0] for item in connected_section]


    def max_connected_section(self, points : List[Point],
            grid_step : float, scale : int) -> dict:
        ''' Returns max connected section within the rect
        '''
        points = [point for point in points if self._rect.contains(point)]
        if len(points) == 0:
            return {
                'cells' : [],
                'point_num' : 0
            }
        min_corner = Point(
            x=min([point.x for point in points]),
            y=min([point.y for point in points])
        )
        max_corner = Point(
            x=max([point.x for point in points]),
            y=max([point.y for point in points])
        )
        candidates = []
        for shift_x in np.arange(0.0, 1.0, grid_step):
            for shift_y in np.arange(0.0, 1.0, grid_step):
                cells = self.distribute(scale, points, min_corner, max_corner,
                    shift_x, shift_y)
                section = self.connected_section(cells)
                candidates.append((section, self.section_points(section)))
        max_connected_section = max(candidates, key=lambda candidate : candidate[1])
        return {
            'cells' : max_connected_section[0],
            'point_num' : max_connected_section[1]
        }


    def section_is_fat(self, section : List[Cell]):
        if len(section) == 0:
            return False
        min_y = min([cell.mat_index[0] for cell in section])
        max_y = max([cell.mat_index[1] for cell in section])
        min_x = min([cell.mat_index[0] for cell in section])
        max_x = max([cell.mat_index[1] for cell in section])
        if min_x == max_x:
            return True
        width_x = (max_x - min_x)
        width_y = (max_y - min_y)
        aspect_ratio = width_y / width_x
        if aspect_ratio <= 1:
            return False
        rect_area = width_x * width_y
        section_area = len(section)
        if section_area / rect_area > 0.5:
            return True
        return False


    def section_points(self, section : List[Cell]) -> float:
        return sum([cell.points_count for cell in section])

    
    def build_graph(self, section : List[Cell], debug=False):
        if len(section) == 0 or self.section_is_fat(section):
            if self.section_is_fat(section):
                print('толстый')
            return FPath(path_cells=[])
        cell0 = min(section, key=lambda cell : cell.center.x)
        equal_cells = [cell for cell in section if cell.center.x == cell0.center.x]
        cell0 = min(equal_cells, key=lambda cell : math.fabs(cell.center.y))

        # step towards direction, to the left and to the right
        # direction % 6 represents one of the 6th directions on hexagon
        paths = [SFAGPath(0, [cell0], 1, None)]
        # dead or finished
        ended_paths : List[SFAGPath] = []
        # main loop
        step_num = 0
        while True:
            step_num += 1
            steps : List[SFAGPath] = []
            dead_ends : List[SFAGPath] = []
            for path in paths:
                path_steps = path.step(section, paths, ended_paths)
                if len(path_steps) == 0:
                    dead_ends.append(path)
                    continue
                steps.extend(path_steps)
            
            paths = [path for path in paths if path not in dead_ends]
            ended_paths.extend(dead_ends)

            while len(dead_ends) > 0:
                path = dead_ends.pop(0)
                if path.dead:
                    continue
                if len(path.neighbors) == 0:
                    path.finish()
                    continue
                all_neighbors_dead_ends = True
                skip = False
                for neighbor in path.neighbors:
                    if (neighbor not in dead_ends):
                        all_neighbors_dead_ends = False
                    if (not neighbor.dead and
                        neighbor not in dead_ends):
                        skip = True
                        break

                if all_neighbors_dead_ends:
                    for neighbor in path.neighbors:
                        neighbor.die()
                    path.finish()
                    skip = True

                finished = False
                for obstacle in path.obstacles:
                    if skip:
                        break
                    if (obstacle not in dead_ends or
                        obstacle.dead):
                        continue
                    obstacle.die()
                    path.finish()
                    finished = True
                if finished:
                    continue
                path.die()

            if len(steps) == 0:
                break
            # find uncontested steps
            passed_steps = []
            contested_steps = steps
            contester_parents = paths
            # проверить упорядоченность по parent-ам
            # contested_steps = [step for step in steps if step not in passed_steps]
            while len(contested_steps) > 0:
                cur = contested_steps[0]
                if len(contester_parents) == 1:
                    passed_steps.extend(contested_steps)
                    break

                passed_steps.append(cur)

                cur_contester_num = sum([step.last == cur.last for step in contested_steps])
                #if uncontested, do not exclude parent from further contests
                if cur_contester_num > 1:
                    contester_parents = [parent for parent in contester_parents if parent.id != cur.parent_id]
                contested_steps = [step for step in contested_steps if step.last != cur.last]
            
            for parent in contester_parents:
                parent.die()
            ended_paths.extend(contester_parents)
            
            for i, step in enumerate(passed_steps):
                step.id = i
            # print('step#', step_num, ', ', len(paths), 'paths,', len(passed_steps), 'p_steps:', passed_steps,
            #     '\n', len(steps), 'steps:', steps)
            paths = passed_steps

        finishers = [path for path in ended_paths if path.finished]

        if len(finishers) == 0:
            return FPath(path_cells=[])

        # удалить заусенцы
        done = False
        while not done:
            branches = {}
            for finisher in finishers:
                for i, cell in reversed(list(enumerate(finisher.cell_path[:-1]))):
                    # begin from pre-last
                    overlaps = [path for path in finishers if cell.mat_index in path]
                    if len(overlaps) > 1:
                        if branches.get(cell):
                            branches[cell].append(finisher.cell_path[(i+1):])
                        else:
                            branches[cell] = [finisher.cell_path[(i+1):]]
                        break

            last_points_to_remove = []
            for _, fork in branches.items():
                if len(fork) > 1:
                    longest_branch = max(fork, key=lambda branch : len(branch))
                    max_len = len(longest_branch)
                    last_points_to_remove.extend([branch[-1] for branch in fork
                        if (len(branch) < 0.4 * max_len or len(branch) <= 1) and branch != longest_branch])
            for point in last_points_to_remove:
                finishers = [path for path in finishers if path.last != point]
            done = len(last_points_to_remove) == 0

        finishers.sort(key=lambda path : len(path.cell_path), reverse=True)
        longest_path = finishers[0]
        finishers_num = len(finishers)

        if finishers_num == 1:
            if debug:
                return [finishers[0].cell_path]
            return FPath(path_cells=finishers[0].cell_path)

        for cell in longest_path.cell_path:
            forked_paths = [path for path in finishers if cell.mat_index not in path]
            if len(forked_paths) > 0:
                second_path = forked_paths[0]
                line, fork1, fork2, fork1len, fork2len = longest_path.adjust_fork(second_path)
                longest_fork = max(fork1len, fork2len)
                if debug:
                    return line, fork1, fork2
                if longest_fork <= 1:
                    return FPath(path_cells=line)
                if fork1len <= 1:
                    return FPath(path_cells=second_path.cell_path)
                if fork2len <= 1:
                    return FPath(path_cells=longest_path.cell_path)
                return FPath(path_cells=line, fork1=fork1, fork2=fork2)

        return finishers


    def init_indices(self, y_count, x_count) -> List[Tuple[int, int]]:
        # (y, x)
        start_y = math.floor(y_count / 2)
        indices = [(start_y, 0)]
        max_dimension = max(x_count, y_count)

        for arch_size in range(1, max_dimension):
            ceil = min(arch_size, x_count)
            left_y = start_y - arch_size
            right_y = start_y + arch_size
            left_edge = max(left_y, 0)
            right_edge = min(right_y, y_count - 1)
            if left_y >= 0:
                left_pillar = [(left_y, x) for x in range(ceil)]
                indices.extend(left_pillar)
            if arch_size < x_count:
                roof = [(y, ceil) for y in range(left_edge, right_edge + 1)]
                indices.extend(roof)
            if right_y < y_count:
                right_pillar = [(right_y, x) for x in range(ceil)]
                indices.extend(right_pillar)
        return indices
