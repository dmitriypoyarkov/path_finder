from typing import Dict, List, Tuple

from matplotlib.cbook import index_of
from matplotlib.style import available
from fpath import FPath
from std_msgs.msg import Header
from geometry_msgs.msg import Pose, PoseStamped, Point
from nav_msgs.msg import Path
from graph_analyzer.srv import GetPath, GetPathResponse, GetPathRequest
from operator import attrgetter
from operator import itemgetter
import rospy
import math
import my_math
import copy
import numpy as np
from enum import Enum
from tf.transformations import euler_from_quaternion
import cv2 as cv

class PatternDetector(object):
    def __init__(self):
        self.H_MIN = np.array((0, 0, 90), np.uint8)
        self.H_MAX = np.array((255, 255, 255), np.uint8)

        self.COLOR_FILL_THRESHOLD = 0.6
        self.DISTRIBUTION_SCALE = 60


    def detect(self, img : np.ndarray) -> List[Point]:
        # делим картинку на квадратики
        # для каждого квадратика, если процент белых больше чем
        # заданная константа, - это точка. добавляем в массив
        # всё.
        thresholded_img = self.threshold_by_color(img)

        height, width = thresholded_img.shape
        diagonal = (height**2 + width**2)**0.5
        size_of_square = int(diagonal / self.DISTRIBUTION_SCALE)
        detected_landmarks : List[Point] = [] # x, y
        x = 0
        while x < width:
            y = 0
            while y < height:
                y_bounds = [y, min(y + size_of_square, height)]
                x_bounds = [x, min(x + size_of_square, width)]
                cur_square = np.array(thresholded_img[y_bounds[0]:y_bounds[1], x_bounds[0]:x_bounds[1]])
                cur_square_area = cur_square.shape[0]*cur_square.shape[1]
                cur_square_pixels = cur_square.reshape((cur_square_area, 1))
                white_pixels = [pixel for pixel in cur_square_pixels if pixel > 0]
                white_pixels_sum = np.sum(white_pixels)
                cur_square_whiteness = float(white_pixels_sum) / (255*cur_square_area)
                if cur_square_whiteness >= self.COLOR_FILL_THRESHOLD:
                    square_coords = [np.average(x_bounds), np.average(y_bounds)]
                    square_point = Point(x=square_coords[0], y=square_coords[1])
                    detected_landmarks.append(square_point)
                y += size_of_square
            x += size_of_square
        result = [landmark for landmark in detected_landmarks if landmark.y > height * 0.52]
        return result


    def threshold_by_color(self, img : np.ndarray) -> np.ndarray:
        hsv = cv.cvtColor(img, cv.COLOR_BGR2HSV)
        thresh = cv.inRange(hsv, self.H_MIN, self.H_MAX)
        return thresh


if __name__ == '__main__':
    detector = PatternDetector()
    img = cv.imread('/home/fc/catkin_ws/src/path_finder/graph_analyzer/img/testline.jpg')
    points = detector.detect(img)
    for point in points:
        cv.circle(img=img, center=(int(point.x),int(point.y)), radius=3, color=(255,0,0), thickness=-1)
    cv.imshow('test', img)
    cv.waitKey(0)

# cv.namedWindow( "result" ) # создаем главное окно
# cv.namedWindow( "settings" ) # создаем окно настроек

# cap = cv.VideoCapture(0)
# # создаем 6 бегунков для настройки начального и конечного цвета фильтра
# cv.createTrackbar('h1', 'settings', 0, 255, nothing)
# cv.createTrackbar('s1', 'settings', 0, 255, nothing)
# cv.createTrackbar('v1', 'settings', 0, 255, nothing)
# cv.createTrackbar('h2', 'settings', 255, 255, nothing)
# cv.createTrackbar('s2', 'settings', 255, 255, nothing)
# cv.createTrackbar('v2', 'settings', 255, 255, nothing)
# crange = [0,0,0, 0,0,0]

# img = cv.imread('/home/fc/catkin_ws/src/path_finder/graph_analyzer/img/testpath.jpg')
# while True:
#     hsv = cv.cvtColor(img, cv.COLOR_BGR2HSV)
 
#     # считываем значения бегунков
#     h1 = cv.getTrackbarPos('h1', 'settings')
#     s1 = cv.getTrackbarPos('s1', 'settings')
#     v1 = cv.getTrackbarPos('v1', 'settings') # 187
#     h2 = cv.getTrackbarPos('h2', 'settings')
#     s2 = cv.getTrackbarPos('s2', 'settings')
#     v2 = cv.getTrackbarPos('v2', 'settings')

#     # формируем начальный и конечный цвет фильтра
#     h_min = np.array((h1, s1, v1), np.uint8)
#     h_max = np.array((h2, s2, v2), np.uint8)

#     # накладываем фильтр на кадр в модели HSV
#     thresh = cv.inRange(hsv, h_min, h_max)

#     cv.imshow('result', thresh) 
 
#     ch = cv.waitKey(5)
#     if ch == 27:
#         break

# cap.release()
# cv.destroyAllWindows()