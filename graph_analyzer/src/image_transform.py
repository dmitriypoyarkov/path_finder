from typing import Tuple

import numpy as np
import rospy
import tf
import tf2_ros
from geometry_msgs.msg import Point
from scipy.spatial.transform import Rotation
from sensor_msgs.msg import CameraInfo


class ImageToWorldProjector(object):
    def __init__(self, cam_data : CameraInfo,
            height : float, orientation : list,
            cam_tf_frame = '/yarp8r2/cam_front_link'):
        self._height = height
        self._cam_info = cam_data
        self._fx = cam_data.K[0]
        self._fy = cam_data.K[4]
        self._cx = cam_data.K[2]
        self._cy = cam_data.K[5]
        self._rotation = Rotation.from_quat(orientation)
        self._rotation = Rotation.from_euler('zyx',
            [0.0, 0.0, 0.04], degrees=False)
        
        tf_listener = tf.TransformListener()
        while True:
            try:
                tf_listener.waitForTransform(cam_tf_frame, 'map', rospy.Time(0), rospy.Duration(0))
                break
            except tf2_ros.TransformException:
                rospy.sleep(0.1)
        (trans, rot) = tf_listener.lookupTransform(cam_tf_frame, 'map', rospy.Time(0))
        self._cam_x = trans[0]
        self._cam_y = trans[1]
        self._cam_z = trans[2]


    def project(self, point : Tuple[np.ndarray, Point], x_inversed = True) -> np.ndarray:
        if isinstance(point, Point):
            point = np.array([point.x, point.y])

        # TODO: Сейчас камера почти не наклонена, и это будет работать.
        px = point[0]
        py = point[1]
    
        vx = (px - self._cx)/self._fx
        vy = 1
        vz = (py - self._cy)/self._fy

        ux, uy, uz = self._rotation.apply(vectors=[vx, vy, vz], inverse=False)

        wx = ux * self._height / uz
        wy = uy * self._height / uz
        wz = 0

        pxN = wx
        pyN = wy - self._cam_x

        if x_inversed:
            return np.array([pyN, -pxN])
        else:
            return np.array([pyN, pxN])
