from typing import List

import rospy
from geometry_msgs.msg import Pose, PoseStamped
from nav_msgs.msg import Path
from std_msgs.msg import Header

import my_math


class SearchCommandSequence(object):
    def __init__(self, turn_sequence : List[int],
            turn_angle : float):
        self._turn_sequence = turn_sequence
        self._turn_angle = turn_angle
        self._cur_command : Path = None
        self._cur_step = 0
    

    @property
    def cur_command(self):
        return self._cur_command
    

    def reset(self):
        self._cur_step = 0
        self._cur_command = None


    def next(self, robot_pose : Pose) -> bool:
        if self._cur_step == len(self._turn_sequence):
            self.reset()
            return False
        target_orientation = my_math.rotate_quaternion(
            robot_pose.orientation,
            self._turn_angle * self._turn_sequence[self._cur_step]
        )
        target_pose = Pose(position=robot_pose.position,
            orientation=target_orientation)
        pose_stamped = PoseStamped(
            header=Header(
                        stamp=rospy.Time.now(),
                        frame_id='map'
                    ),
            pose=target_pose
        )
        self._cur_command = Path(poses = [pose_stamped])
        self._cur_step += 1
        return True
