from collections import defaultdict
from importlib.machinery import PathFinder
from typing import List

import rospy
from std_msgs.msg import UInt8

from pf_client1 import PathFinderClient


class NetControl(object):
    def __init__(self, client : PathFinderClient,
            net : List[List[int]],
            cur_position : int):
        self._client = client

        self._net = net
        self._cur_position = cur_position

        self._move_progress = 0
        self._turn_sequence = None
        self._client.add_fork_callback(self.fork_choice_callback)
        self._cur_goal = None

        self._position_pub = rospy.Publisher('~cur_position', UInt8)
        rospy.Timer(rospy.Duration(0.1), self.publish_position, oneshot=False)


    @property
    def net(self):
        return self._net
    

    @net.setter
    def net(self, value : List[List[int]]):
        self._net = value


    @property
    def vertex_num(self):
        max_vertex = 0
        for vertex in self._net:
            for neighbor in vertex:
                if neighbor > max_vertex:
                    max_vertex = neighbor
        return max_vertex + 1


    @property
    def cur_position(self):
        return self._cur_position
    

    @cur_position.setter
    def cur_position(self, value : int):
        self._cur_position = value


    def publish_position(self, _):
        self._position_pub.publish(UInt8(data=self._cur_position))


    def get_turn_sequence(self, start : int, end : int):
        vertices = self.get_vertex_sequence(start, end)
        steps = self._net[vertices[0]]
        if len(steps) > 1:
            raise Exception('Can\'t start from fork')
        prev_vertex = vertices[0]
        turns = []
        for i in range(1, len(vertices) - 1):
            prev_vertex = vertices[i-1]
            cur_vertex = vertices[i]
            next_vertex = vertices[i+1]
            neighbors = self._net[cur_vertex]
            prev_index = neighbors.index(prev_vertex)
            turns_sorted = neighbors[(prev_index + 1):] + neighbors[:prev_index]
            turn_index = turns_sorted.index(next_vertex)
            turns.append(turn_index)
        return turns


    def get_vertex_sequence(self, start, goal):
        explored = []
        
        # Queue for traversing the
        # graph in the BFS
        queue = [[start]]

        if start == goal:
            print("Same Node")
            return
        
        # Loop to traverse the graph
        # with the help of the queue
        while queue:
            path = queue.pop(0)
            node = path[-1]

            if node not in explored:
                neighbours = self._net[node]

                for neighbour in neighbours:
                    new_path = list(path)
                    new_path.append(neighbour)
                    queue.append(new_path)

                    if neighbour == goal:
                        return new_path
                explored.append(node)
 
        raise Exception('Can\'t find path')


    def init_movement(self, end : int):
        self._move_progress = 0
        self._turn_sequence = self.get_turn_sequence(
            self._cur_position,
            end
        )
        print(f'DEBUG Net control: from {self._cur_position} to {end}: {self._turn_sequence}')
        self._cur_goal = end
        self._client.send_start_command()


    def fork_choice_callback(self, req : UInt8):
        # Assuming we moved to the next waypoint

        print('DEBUG Net control: fork callback')
        self._client.send_turn(
            self._turn_sequence[self._move_progress]
        )

        self._move_progress += 1
        if self._move_progress >= len(self._turn_sequence):
            self._cur_position = self._cur_goal
            self._cur_goal = None


if __name__ == "__main__":
    edges = [
            [1, 2], [2, 3],
            [2, 4], [4, 5],
            [4, 6], [6, 7],
            [6, 8]
        ]
    client = PathFinderClient()
    a = NetControl(
        client,
        net=[[1], [3, 2, 0], [1], [4, 5, 1], [3], [7, 6, 3], [5], [5]],
        cur_position=0
    )
    a.init_movement(6)