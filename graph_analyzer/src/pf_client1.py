import rospy
from actionlib import SimpleActionClient
from graph_analyzer.msg import (PathFinderAction, PathFinderActionGoal,
                                PathFinderGoal)
from graph_analyzer.srv import (ForkChoice, ForkChoiceRequest,
                                ForkChoiceResponse)
from rospy import Header
from sensor_msgs.msg import Image
from std_msgs.msg import UInt8


class PathFinderClient(object):
    def __init__(self):
        rospy.init_node('pf_client', log_level=rospy.DEBUG)
        self._fork_pub = None
        self._action_client = SimpleActionClient('/global_planner_service_node/path_finder', PathFinderAction)
        self._fork_pub = rospy.Publisher('/global_planner_service_node/fork_choice', UInt8, queue_size=1)
        self._frame_sub = None
        self._fork_subs = []
        self._done_cb = lambda : None


    def set_done_cb(self, cb : callable):
        self._done_cb = cb


    def add_fork_callback(self, callback : callable):
        self._fork_subs.append(rospy.Subscriber('/pf_client_sub', UInt8, callback, queue_size=1))


    def send_turn(self, turn : int):
        self._fork_pub.publish(UInt8(data=turn))


    def send_stop_command(self):
        self._action_client.cancel_goal()


    def wait_for_server(self):
        print('DEBUG server waiting')
        self._action_client.wait_for_server()
        print('DEBUG server online')


    def send_start_command(self):
        self._action_client.wait_for_server()
        self._action_client.send_goal(PathFinderGoal(
                fork_choice_mode=PathFinderGoal.USER_CHOICE
            ),
            self._done_cb
        )


    def set_frame_callback(self, callback : callable):
        self._frame_sub and self._frame_sub.unregister()
        self._frame_sub = rospy.Subscriber('/global_planner_service_node/keypoint_img', Image, callback)
