import math
from decimal import DivisionByZero
from typing import List, Union

import cv2 as cv
import numpy as np
import numpy.matlib as npm
import shapely.geometry as sh
import tf2_geometry_msgs as tfg
from geometry_msgs.msg import (Point, Quaternion, Transform, TransformStamped,
                               Vector3, Vector3Stamped)
from numba import float32, jit
from tf.transformations import (euler_from_quaternion, quaternion_about_axis,
                                quaternion_conjugate, quaternion_from_euler,
                                quaternion_multiply, quaternion_inverse)

import rostools

VECTOR_30 = np.array([-0.5, 0.866], np.float32)
PI_3 = np.array(np.pi / 3, np.float32)
PI_6 = np.array(np.pi / 6, np.float32)


def sqr_length(point : Point) -> float:
    return point.x**2 + point.y**2 + point.z**2


@jit(float32(float32[:], float32[:]), nopython=True)
def sqr_distance_2D_np(p1, p2):
    return (p1[0] - p2[0])**2 + (p1[1] - p2[1])**2


def sqr_distance(p1 : Point, p2 : Point) -> float:
    return (p1.x - p2.x)**2 + (p1.y - p2.y)**2 + (p1.z - p2.z)**2


def project_on_line(point : Point, line_point1 : Point,
                    line_point2 : Point) -> Point:
    x0 = point.x
    y0 = point.y
    x1 = line_point1.x
    y1 = line_point2.y
    xb = line_point2.x
    yb = line_point2.y

    if line_point1 == line_point2:
        raise Exception('my_math: project_on_line: points are the same!')
    if line_point1 == point:
        return line_point1
    if line_point2 == point:
        return line_point1
    
    m = xb - x1
    p = yb - y1
    x = (x0 * pow(m, 2) + p * (x1 * p + m * (y0 - y1))) / (pow(m, 2) + pow(p, 2))
    y = (x0 * m * p - x1 * m * p + y0 * pow(p, 2) + pow(m, 2) * y1) / (pow(m, 2) + pow(p, 2))

    return Point(x=x,y=y)


def substract(point1 : Point, point2 : Point) -> Point:
    return Point(x = point1.x - point2.x,
                 y = point1.y - point2.y,
                 z = point1.z - point2.z)


def add(point1 : Point, point2 : Point) -> Point:
    return Point(x = point1.x + point2.x,
                 y = point1.y + point2.y,
                 z = point1.z + point2.z)


def average2D(points : List[Point]) -> Point:
    avg_x = np.average([point.x for point in points])
    avg_y = np.average([point.y for point in points])
    return Point(x=avg_x, y=avg_y)


def divide(point : Point, divider : float) -> Point:
    if divider == 0:
        raise DivisionByZero('my_math: divide: division by zero!')
    return Point(x = point.x / divider,
                 y = point.y / divider,
                 z = point.z / divider)


def multiply(point : Point, multiplier : float) -> Point:
    return Point(x = point.x * multiplier,
                 y = point.y * multiplier,
                 z = point.z * multiplier)


def points_on_different_sides(center : Point,
                              point1 : Point, point2 : Point) -> bool:
    if point1 == point2:
        raise Exception('my_math: points_on_different_sides: points are the same!')
    relative1 = substract(point1, center)
    relative2 = substract(point2, center)
    cond1 = relative1.x >= 0 and relative2.x <= 0
    cond2 = relative1.x <= 0 and relative2.x >= 0
    cond3 = relative1.y >= 0 and relative2.y <= 0
    cond4 = relative1.y <= 0 and relative2.y >= 0
    if (cond1 or cond2) and (cond3 or cond4):
        return True
    return False


def length2D(point : Point) -> float:
    if point.z != 0:
        raise Exception('my_math: length2D: point.z is not zero!')
    return np.sqrt(point.x**2 + point.y**2)

def distance2D(p1 : Point, p2 : Point) -> float:
    return length2D(substract(p1, p2))

def vector_angle2D(vec : Point) -> float:
    if vec.x == 0 and vec.y == 0:
        return 0
    return np.arctan2(vec.y, vec.x)


def angle_between_vectors2D(vec1 : Point, vec2 : Point) -> float:
    angle = math.atan2(vec2.y, vec2.x) - math.atan2(vec1.y, vec1.x)
    if angle > math.pi:
        return angle - 2 * math.pi
    if angle < -math.pi:
        return angle + 2 * math.pi
    return angle


@jit(float32(float32[:], float32[:]), nopython=True)
def angle_between_vectors2D_np(vec1, vec2):
    angle = np.arctan2(vec2[1], vec2[0]) - np.arctan2(vec1[1], vec1[0])
    if angle > np.pi:
        return angle - 2 * np.pi
    if angle < -np.pi:
        return angle + 2 * np.pi
    return angle


def vector_quaternion2D(vec : Point) -> Quaternion:
    angle = vector_angle2D(vec)
    quaternion_tf = quaternion_from_euler(0,0,angle)
    quaternion_gmsgs = Quaternion(x = quaternion_tf[0],
                                  y = quaternion_tf[1],
                                  z = quaternion_tf[2],
                                  w = quaternion_tf[3])
    return quaternion_gmsgs


def angle_3points(p1 : Point, p2 : Point, p3 : Point) -> float:
    vec1 = substract(p1, p2)
    vec2 = substract(p3, p2)
    return angle_between_vectors2D(vec1, vec2)

# if there are two intersections, returns the one closest to the p1.
def circle_section_intersection(circle_center : Point, radius : float,
                                p1 : Point, p2 : Point) -> Union[None, Point] :
    x0 = circle_center.x
    y0 = circle_center.y
    r = radius

    x1 = p1.x
    y1 = p1.y
    x2 = p2.x
    y2 = p2.y

    x10 = x1 - x0
    x21 = x2 - x1

    y10 = y1 - y0
    y21 = y2 - y1

    l = x21
    m = y21

    b = 2 * l * x10 + 2 * m * y10
    a = l**2 + m**2
    c = x10**2 + y10**2 - r**2

    D = b**2 - 4 * a * c

    if D < 0:
        return None
    
    if D == 0:
        t = -b
        res = Point(x = l * t + x1, y = m * t + y1)
        if not points_on_different_sides(res, p1, p2):
            return None
        return res
    
    t1 = (- b + D**0.5)/(2 * a)
    t2 = (- b - D**0.5)/(2 * a)

    res1 = Point(x = l * t1 + x1, y = m * t1 + y1)
    res2 = Point(x = l * t2 + x1, y = m * t2 + y1)
    
    res1_on_section = points_on_different_sides(res1, p1, p2)
    res2_on_section = points_on_different_sides(res2, p1, p2)

    if res1_on_section and res2_on_section:
        distance1 = distance2D(res1, p1)
        distance2 = distance2D(res2, p1)
        if distance1 <= distance2:
            return res2
        return res1
    
    if res1_on_section:
        return res1
    if res2_on_section:
        return res2
    
    return None


def rotate_point(center : Point, point : Point, angle : float) -> Point:
    relative_point = Point(
        x=point.x - center.x,
        y=point.y - center.y
    )
    rotated_point = Point(
        x=center.x + relative_point.x * np.cos(angle) - relative_point.y * np.sin(angle),
        y=center.y + relative_point.x * np.sin(angle) + relative_point.y * np.cos(angle)
    )
    return rotated_point


def vector_towards_quaternion(q : Quaternion):
    return rotate_2d_point_gmsgs(
        Point(x=1, y=0, z=0),
        q
    )

    
def extract_yaw(orientation : Quaternion) -> float:
    euler = euler_from_quaternion([
        orientation.x,
        orientation.y,
        orientation.z,
        orientation.w
    ])
    return euler[2]


def rotate_2d_point_gmsgs(point: Point, rotation: Quaternion) -> Point:
    euler_angles = euler_from_quaternion([rotation.x, rotation.y, rotation.z, rotation.w])
    contrary_rotation = quaternion_from_euler(euler_angles[0], euler_angles[1], -euler_angles[2])
    contrary_rotation = Quaternion(x=contrary_rotation[0], y=contrary_rotation[1], z=contrary_rotation[2], w=contrary_rotation[3])
    result_vector3 = tfg.do_transform_vector3(Vector3Stamped(vector=Vector3(
        x=point.x, y=point.y, z=0)), TransformStamped(transform=Transform(rotation=rotation)))
    return Point(x=result_vector3.vector.x, y=result_vector3.vector.y)


def keypoints_to_arr(keypoints: List[cv.KeyPoint]) -> List[np.ndarray]:
    point_list = np.array([[keypoint.pt[0], keypoint.pt[1]]
                  for keypoint in keypoints])
    return point_list


def rotate_vector(quaternion : Quaternion, vector : list):
    quaternion2 = vector + [0.0]
    return quaternion_multiply(
        quaternion_multiply(quaternion, quaternion2), 
        quaternion_conjugate(quaternion)
    )[:3]


def rotate_quaternion(quaternion : Quaternion, angle : float,
        axis='z') -> Quaternion:
    if axis != 'z':
        print('MY MATH: Not implemented')
        return
    quat = [quaternion.x,
        quaternion.y,
        quaternion.z,
        quaternion.w]
    mult = quaternion_about_axis(angle, (0, 0, 1))
    result = quaternion_multiply(mult, quat)
    mult_gmsgs = Quaternion(
        x=result[0],
        y=result[1],
        z=result[2],
        w=result[3]
    )
    return mult_gmsgs


def orientation(p,q,r):
    '''Return positive if p-q-r are clockwise, neg if ccw, zero if colinear.'''
    return (q[1]-p[1])*(r[0]-p[0]) - (q[0]-p[0])*(r[1]-p[1])


def hulls(Points):
    '''Graham scan to find upper and lower convex hulls of a set of 2d points.'''
    U = []
    L = []
    Points.sort()
    for p in Points:
        while len(U) > 1 and orientation(U[-2],U[-1],p) <= 0: U.pop()
        while len(L) > 1 and orientation(L[-2],L[-1],p) >= 0: L.pop()
        U.append(p)
        L.append(p)
    return U,L


def rotatingCalipers(Points):
    '''Given a list of 2d points, finds all ways of sandwiching the points
between two parallel lines that touch one point each, and yields the sequence
of pairs of points touched by each pair of lines.'''
    U,L = hulls(Points)
    i = 0
    j = len(L) - 1
    while i < len(U) - 1 or j > 0:
        yield U[i],L[j]
        
        # if all the way through one side of hull, advance the other side
        if i == len(U) - 1: j -= 1
        elif j == 0: i += 1
        
        # still points left on both lists, compare slopes of next hull edges
        # being careful to avoid divide-by-zero in slope calculation
        elif (U[i+1][1]-U[i][1])*(L[j][0]-L[j-1][0]) > \
                (L[j][1]-L[j-1][1])*(U[i+1][0]-U[i][0]):
            i += 1
        else: j -= 1


def diameter(Points):
    '''Given a list of 2d points, returns the pair that's farthest apart.'''
    diam,pair = max([((p[0]-q[0])**2 + (p[1]-q[1])**2, (p,q))
                     for p,q in rotatingCalipers(Points)])
    return pair


def path_len(path : List[Point]):
    if len(path) <= 1:
            return 0.0
    length = 0.0
    for i in range(1, len(path)):
        length += (path[i] - path[i - 1]).magnitude2D()
    return length


def path_diff(p1 : List[Point], p2 : List[Point]):
    distance_sum = 0.0
    total_points = 0
    for i in range(2):
        paths = [p1, p2]
        paths.sort(key=path_len)
        for point in paths[0]:
            distance = min([(point - other).magnitude2D()
                for other in paths[1]])
            distance_sum += distance
        total_points += paths[0]
    return distance_sum


def average_quaternions(Q):
    # Number of quaternions to average
    Q = np.array(Q)
    M = Q.shape[0]
    A = npm.zeros(shape=(4,4))

    for i in range(0,M):
        q = Q[i,:]
        # multiply q with its transposed version q' and add A
        A = np.outer(q,q) + A

    # scale
    A = (1.0/M)*A
    # compute eigenvalues and -vectors
    eigenValues, eigenVectors = np.linalg.eig(A)
    # Sort by largest eigenvalue
    eigenVectors = eigenVectors[:,eigenValues.argsort()[::-1]]
    # return the real part of the largest eigenvector (has only real part)
    return np.real(eigenVectors[:,0].A1)


# Average multiple quaternions with specific weights
# The weight vector w must be of the same length as the number of rows in the
# quaternion maxtrix Q
def weighted_average_quaternions(Q, w):
    # Number of quaternions to average
    M = Q.shape[0]
    A = npm.zeros(shape=(4,4))
    weightSum = 0

    for i in range(0,M):
        q = Q[i,:]
        A = w[i] * np.outer(q,q) + A
        weightSum += w[i]

    # scale
    A = (1.0/weightSum) * A

    # compute eigenvalues and -vectors
    eigenValues, eigenVectors = np.linalg.eig(A)

    # Sort by largest eigenvalue
    eigenVectors = eigenVectors[:,eigenValues.argsort()[::-1]]

    # return the real part of the largest eigenvector (has only real part)
    return np.real(eigenVectors[:,0].A1)
