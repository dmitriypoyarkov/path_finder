# region imports
import math
import time
from operator import itemgetter
from threading import Lock
from typing import List, Tuple, Union

import numpy as np
import rospy
from actionlib import SimpleActionClient, SimpleActionServer
from actionlib_msgs.msg import GoalStatus
from geometry_msgs.msg import (Point, PointStamped, Pose, PoseStamped,
                               Quaternion, Vector3)
from graph_analyzer.msg import (PathFinderAction, PathFinderActionGoal,
                                PathFinderGoal, PathFinderResult)
from graph_analyzer.srv import GetPath, GetPathRequest, GetPathResponse
from map_msgs.msg import OccupancyGridUpdate
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from nav_msgs.msg import MapMetaData, OccupancyGrid, Path
from scipy.spatial import ConvexHull
from shapely.geometry import Polygon
from std_msgs.msg import ColorRGBA, Header, UInt8
from visualization_msgs.msg import Marker
from graph_analyzer.msg import PathFinderState

import my_math
import rostools
from debug_manager import DebugManager
from fpath import FPath
from obstacle import Multiobstacle, Obstacle
from path_detector import FPathDetector
from ray import Ray
from search_command_sequence import SearchCommandSequence
from spath import SPath
from spath_main import SPathMain
from system_state import State, StateMachine

# endregion


class PathHandler(object):
    def __init__(self, fpath_detector : FPathDetector, move_base_action_server : str,
            debug : DebugManager = None):
        self._fpath_detector = fpath_detector
        self._debug = debug

        # params
        self._BASE_SCALE = 0.10
        self.INIT_PLAN_STEP = 0.3 * self._BASE_SCALE
        self.OBSTACLE_STEP = 0.7 * self._BASE_SCALE
        self.GOAL_TOLERANCE = self._BASE_SCALE
        self.FORK_DIFF_TOLERANCE = 1.5 * self._BASE_SCALE

        self.POINT_TOLERANCE = 1.3 * self._BASE_SCALE
        self.PATH_INNER_RADIUS = self.GOAL_TOLERANCE
        self.PATH_OUTER_RADIUS = 3.4 * self.PATH_INNER_RADIUS
        self.TURN_STEP = 2.5 * self._BASE_SCALE

        self.VIEWING_ANGLE = 0.75
        self.BASE_SMOOTH_SCALE = 0.05
        self.SMOOTH_SCALE = self.BASE_SMOOTH_SCALE
        self.MAX_MOVE_ATTEMPTS = 10
        self.FORK_PASS_FRAMES = 10
        self.MOVE_BASE_GOAL = MoveBaseGoal(
            target_pose=PoseStamped(
                header=Header(
                    stamp=rospy.Time.now(),
                    frame_id='map'
                ),
                pose=Pose(
                    orientation=Quaternion(w=1)
                )
            )
        )

        # variables
        self.reset_stored_plan()
        self._lock = Lock()

        self._direction_point : Point = None
        self._previous_fpath_with_fork : FPath = None
        self._stored_fpath : FPath = None

        self._turn_index : int = None

        self._motion_check = False
        self._unsuccessfull_move_attempts = 0

        self._searching_command = None
        self._cur_obstacle = None
        self._costmap = None
        self._frames_without_fork = 0

        self._state = StateMachine.IDLE()
        self._plan_step = self.INIT_PLAN_STEP

        # Publishers, subscribers, services, actions
        # self._full_plan_pub = rospy.Publisher('~full_plan', Path, queue_size=2)
        self._move_base_client = SimpleActionClient(move_base_action_server, MoveBaseAction)
        self._action_server = SimpleActionServer('~path_finder', PathFinderAction,
            self.action_callback, auto_start=False)
        self._action_server.start()

        rospy.Service('global_planner_service', GetPath, self.getpath_callback)

        self._fork_choice_pub = rospy.Publisher('/pf_client_sub', UInt8, queue_size=2)
        self._obstacle_pub = rospy.Publisher('~obstacle', Marker, queue_size=1)

        rospy.Subscriber('~fork_choice', UInt8, self.fork_choice_callback)
        rospy.Subscriber('/yarp8r2/motion/move_base/local_costmap/costmap',
            OccupancyGrid, self.save_costmap_callback, queue_size=1)
        rospy.Subscriber('/yarp8r2/motion/move_base/local_costmap/costmap_updates',
            OccupancyGridUpdate, self.update_costmap_callback, queue_size=1)

        rospy.Timer(rospy.Duration(0.1), self.analyze_obstacles)
        rospy.Timer(rospy.Duration(0.07), self.direction_maintainer)
        # rospy.Timer(rospy.Duration(0.2), self.publish_obstacle)

        # debug publishers
        self._vector_forward_debug = None
        self._new_segment_pub = rospy.Publisher('~new_segment', Path, queue_size=1)
        self._new_fork_pub1 = rospy.Publisher('~new_fork1', Path, queue_size=1)
        self._new_fork_pub2 = rospy.Publisher('~new_fork2', Path, queue_size=1)
        self._new_segment_pubp = rospy.Publisher('~new_segmentp', Path, queue_size=1)
        self._new_fork_pub1p = rospy.Publisher('~new_fork1p', Path, queue_size=1)
        self._new_fork_pub2p = rospy.Publisher('~new_fork2p', Path, queue_size=1)
        self._origin_pub = rospy.Publisher('~segment_origin', Marker, queue_size=1)
        self._debug_marker1_pub = rospy.Publisher('~debug_marker1', Marker, queue_size=1)

        self._debug_state_pub = rospy.Publisher('~path_finder_state', PathFinderState, queue_size=1)
        rospy.Timer(rospy.Duration(0.1), self.publish_state_debug, oneshot=False)
        rospy.Timer(rospy.Duration(0.1), self.direction_publisher_debug, oneshot=False)

        rospy.logdebug('PATH HANDLER: Inited!')


    @property
    def turn_mode(self):
        return self._turn_mode
    

    @turn_mode.setter
    def turn_mode(self, value):
        self._turn_mode = value

    
    @property
    def motion_check(self):
        return self._motion_check
    

    @motion_check.setter
    def motion_check(self, value):
        if value == True:
            self._unsuccessfull_move_attempts = 0
        self._motion_check = value


    @property
    def frames_without_fork(self):
        return self._frames_without_fork


    @frames_without_fork.setter
    def frames_without_fork(self, value):
        print('DEBUG fork val', value)
        if value > self.FORK_PASS_FRAMES:
            print('DEBUG fork passed')
            self._state = StateMachine.PATH_FOLLOW()
            self._direction_point = None
            self._frames_without_fork = 0
            self.SMOOTH_SCALE = self.BASE_SMOOTH_SCALE
            return
        self._frames_without_fork = value


    def action_callback(self, goal : PathFinderGoal):
        self._turn_mode = goal.fork_choice_mode
        self.reset_stored_plan()
        self.setup_turn_index(goal.fork_choice_mode)
        self._state = StateMachine.START()
        print('DEBUG Команда start получена')
        while self.action_loop():
            rospy.sleep(rospy.Duration(0.05))
        print('DEBUG приехали')


    def action_loop(self):
        if self._state.start:
            self._state = StateMachine.SEARCH(StateMachine.PATH_FOLLOW())
            print('Начинаем поиск1')
            self._searching_command = SearchCommandSequence(
                [-1, -1], 3.13
            )
            self._searching_command.next(
                self._fpath_detector.get_robot_pose()
            )
            self.send_move_base_goal()

        if self._state.finish:
            self._state = StateMachine.SEARCH(self._state.previous_state)
            print('Начинаем поиск')

            self._searching_command = SearchCommandSequence(
                [-1, 1, 1, -1],
                0.8
            )
            self._searching_command.next(
                self._fpath_detector.get_robot_pose()
            )
            self.send_move_base_goal()

        if self._action_server.is_preempt_requested():
            self.cancel_move_base_goal()
            self._action_server.set_preempted(self.action_result())
            self._state = StateMachine.IDLE()
            return False

        if self._state.lost:
            self.action_server_abort()
            return False

        if self._state.idle:
            return False

        return True


    def move_base_done_cb(self, state, result):
        print('DEBUG move base done')
        if self._state.path_follow:
            self._state = StateMachine.FINISH(self._state)
        if self._state.search:
            print('Докрутились')
            next_command_present = self._searching_command.next(
                        self._fpath_detector.get_robot_pose())
            if not next_command_present:
                self.action_server_succeed()
                self._state = StateMachine.IDLE()
                print('Вроде как приехали')
                return
            self.send_move_base_goal(wait=False)
        if self._state.directing:
            rospy.logwarn('Потерялись')
            self._state = StateMachine.LOST()
            return


    def action_server_abort(self):
        self._action_server.set_aborted(
            PathFinderResult(
                success=False,
                position=PointStamped(
                    header=Header(
                        stamp=rospy.Time.now(),
                        frame_id='base_link'
                    ),
                    point=self._fpath_detector.get_robot_pose().position
                )
            )
        )


    def action_server_succeed(self):
        self._action_server.set_succeeded(
            PathFinderResult(
                success=True,
                position=PointStamped(
                    header=Header(
                        stamp=rospy.Time.now(),
                        frame_id='base_link'
                    ),
                    point=self._fpath_detector.get_robot_pose().position
                )
            )
        )


    def cancel_move_base_goal(self):
        self._move_base_client.cancel_goal()
        status = self._move_base_client.get_state()
        while status not in [GoalStatus.SUCCEEDED,
                GoalStatus.ABORTED, GoalStatus.PREEMPTED]:
            if status == GoalStatus.ACTIVE:
                self._move_base_client.cancel_goal()
            status = self._move_base_client.get_state()
            print('Waiting for cancel...', status)
            rospy.sleep(0.1)


    def send_move_base_goal(self, wait=True):
        self._move_base_client.send_goal(
            self.MOVE_BASE_GOAL,
            self.move_base_done_cb
        )
        if not wait:
            return
        waiting_steps = 0
        state = self._move_base_client.get_state()
        while state not in [GoalStatus.ACTIVE, GoalStatus.SUCCEEDED]:
            state = self._move_base_client.get_state()
            print('DEBUG Waiting for move base goal...', state)
            waiting_steps += 1
            if waiting_steps >= 10:
                self._move_base_client.send_goal(
                    self.MOVE_BASE_GOAL,
                    self.move_base_done_cb
                )
                waiting_steps = 0
            rospy.sleep(0.1)


    def setup_turn_index(self, turn_mode : int):
        if turn_mode == PathFinderGoal.ALWAYS_LEFT:
            self._turn_index = -1
        if turn_mode == PathFinderGoal.ALWAYS_LEFT:
            self._turn_index = 0
        if turn_mode == PathFinderGoal.USER_CHOICE:
            self._turn_index = None


    def save_costmap_callback(self, costmap : OccupancyGrid):
        self._costmap = costmap
    

    def update_costmap_callback(self, costmap : OccupancyGridUpdate):
        #requires publish_full_costmap to be true
        if not self._costmap:
            return
        self._costmap.data = costmap.data


    def analyze_obstacles(self, _):
        # at first assume we can have only one obstacle
        if not self._costmap:
            return
        width = self._costmap.info.width
        obstacle_squares = []
        for i, value in enumerate(self._costmap.data):
            if value > 90:
                r = i // width
                c = i % width
                obstacle_squares.append(
                    Polygon([
                        tuple(self._costmap.index_to_world_lst(r, c)),
                        tuple(self._costmap.index_to_world_lst(r + 1, c)),
                        tuple(self._costmap.index_to_world_lst(r + 1, c + 1)),
                        tuple(self._costmap.index_to_world_lst(r, c + 1))
                    ])
                )
        if len(obstacle_squares) == 0:
            return
        self._cur_obstacle = Multiobstacle(obstacle_squares)
        self._state = StateMachine.OBSTACLE_MOVE()


    def check_motion(self):
        if len(self._main_path) > 0:
            print('DEBUG main path', len(self._main_path),
                    (self._main_path[-1] -
                    self._fpath_detector.get_robot_pose().position).magnitude2D())
        if (len(self._main_path) > 0 and
                (self._main_path[-1] -
                self._fpath_detector.get_robot_pose().position).magnitude2D()
                > self.GOAL_TOLERANCE):
            self.motion_check = True

        if not self.motion_check:
            self._unsuccessfull_move_attempts += 1
            if self._unsuccessfull_move_attempts >= self.MAX_MOVE_ATTEMPTS:
                print('Финишируем')
                self._unsuccessfull_move_attempts = 0
                self._state = StateMachine.FINISH(self._state)
                self.cancel_move_base_goal()
            print('Стоим', self._unsuccessfull_move_attempts, 'шагов')
        self.motion_check = False


    def get_robot_pose(self):
        return self._fpath_detector.get_robot_pose()


    def stop_robot_path(self):
        return Path(
            header=Header(
                    stamp=rospy.Time.now(),
                    frame_id='map'
                ),
            poses=[
                PoseStamped(
                    header=Header(
                        stamp=rospy.Time.now(),
                        frame_id='map'
                    ),
                    pose=self.get_robot_pose()
                )
            ]
        )


    def getpath_callback(self, req: GetPathRequest) -> GetPathResponse:
        # # DEBUG
        print('DEBUG getpath')
        if not self._state.getpath_callback:
            raise Exception(f'?????: {self._state.__dict__}')

        if self._state.obstacle_move:
            # TODO: здесь доделать надо, ошибку выдаст
            path = self.build_obstacle_avoiding_path()
        if self._state.search:
            if self.valid_path_visible():
                print('DEBUG поиск прекращён, сопровождаем')
                self._state = self._state.previous_state
            else:
                print('DEBUG Ищем...')
                path = self.build_searching_path()
        if self._state.path_follow:
            print('DEBUG Сопровождаем...')
            path = self.build_following_path()
            self.check_motion()
        if self._state.directing:
            print('DEBUG Доворачиваем...')
            path = self.build_directing_path()
            self.refresh_directing_point()
            if self.direction_deviation_angle(self.get_robot_pose()) < 0.11:
                print('DEBUG Довернули')
                self.SMOOTH_SCALE += self.BASE_SMOOTH_SCALE
                self._state = self._state.previous_state
        return GetPathResponse(path=path)


    def refresh_directing_point(self):
        origin = self._fpath_detector.get_origin(
            SPath(self._main_path.path)
        )
        fpath = self._fpath_detector.receive_and_detect(
            origin
        )
        # Assuming we can be in this state only from fork_move
        if fpath.has_fork:
            if fpath.fork_diff(self._previous_fpath_with_fork) < self.FORK_DIFF_TOLERANCE:
                print('DEBUG Direction state, detected same fork')
                self._previous_fpath_with_fork = fpath
                self._direction_point = fpath.path[-1]
                return
            rospy.logwarn('Direction state, detected different fork!')
        print('DEBUG Direction state, no fork yet')


    def build_directing_path(self):
        return self._searching_command.cur_command


    def valid_path_visible(self):
        fpath = self._fpath_detector.receive_and_detect()
        # self.publish_marker1_debug(self._fpath_detector._nearest_point_debug)
        self.publish_fpath_debug(fpath)
        return self.path_is_valid(fpath)


    def path_is_valid(self, fpath : FPath, origin : Point = None):
        if len(fpath.path) == 0:
            return False
        if origin is None:
            origin = fpath.start_point
        print('DEBUG len', fpath.path.calculate_length())
        if fpath.begin_distance(origin) > self.PATH_OUTER_RADIUS:
            return False
        if fpath.end_distance(origin) < self.PATH_INNER_RADIUS:
            return False
        if fpath.path.calculate_length() < self._BASE_SCALE:
            return False
        return True


    def build_searching_path(self):
        return self._searching_command.cur_command


    def build_following_path(self):
        origin = self._fpath_detector.get_origin(
            SPath(self._main_path.path)
        )
        fpath = self._fpath_detector.receive_and_detect(
            origin
        )

        fpath.smooth(0.02, self._plan_step)
        # DEBUG
        self.publish_prev_fpath_debug(self._previous_fpath_with_fork)
        self.publish_fpath_debug(fpath)
        # self.publish_origin_debug(origin, self.get_robot_pose())
        # self.publish_marker1_debug(self._fpath_detector._nearest_point_debug)

        if fpath.has_fork and not self._state.fork_move:
            print('DEBUG fork detected')
            self._state = StateMachine.FORK_CHOICE_WAIT()
            self.cancel_move_base_goal()
            self._fork_choice_pub.publish(UInt8(data=len(fpath.fork_steps)))
            self._stored_fpath = fpath
            self._previous_fpath_with_fork = fpath

            self._direction_point = fpath.path[-1]
            return self.stop_robot_path()

        if fpath.has_fork and self._state.fork_move:
            if self._stored_fpath:
                fpath = self._stored_fpath
                self._stored_fpath = None
            elif fpath.fork_diff(self._previous_fpath_with_fork) > self.FORK_DIFF_TOLERANCE:
                    print('DEBUG different fork')
                    self.frames_without_fork += 1
                    return self.get_plan()
            print('DEBUG same fork')

            self.frames_without_fork = 0
            self._previous_fpath_with_fork = fpath
            self._direction_point = fpath.path[-1]

            # self._main_path.build_on(fpath, turn=self._turn_index, replace=True)
            self._main_path.build_on(fpath, turn=self._turn_index)

        if not fpath.has_fork and self._state.fork_move:
            print('DEBUG without fork')
            # Check if new data is close to the existing path
            
            if fpath.path.continues(self._main_path, self.POINT_TOLERANCE, 0,
                    strict=True):
                print('Фиксируем прибыль')
                self._main_path.build_on(fpath)
            self.frames_without_fork += 1

        if (not self._state.fork_move
                and fpath.path.continues(self._main_path, 2 * self.POINT_TOLERANCE, 0)):
            print('Движемся по-обычному')
            self._main_path.build_on(fpath)

        return self._main_path.path_gmsgs


    def fork_choice_callback(self, msg : UInt8):
        if not self._state.fork_callback:
            print('Invalid state for sending fork choice')
            return
        self._turn_index = msg.data
        self._state = StateMachine.FORK_MOVE()
        self.send_move_base_goal()
        print('Fork choice callback invoked')


    def fork_difference(self, fpath : FPath, path, fork_steps):
        if len(path) == 0:
            return 0.0
        lst1 = [fpath.last_step, *fpath.fork_steps]
        lst2 = [path[-1], *fork_steps]
        return np.average(
                [my_math.distance2D(lst1[i], lst2[i])
                    for i in range(len(lst1))]
            )

    #debugd
    def point_list_to_str(self, points : List[Point]):
        string = ''
        for point in points:
            string += f'({point.x:.2f}, {point.y:.2f}) '
        return string


    def prepare_to_publish(self, fpath : FPath) -> Path:
        fpath.smooth(self.SMOOTH_SCALE, step=self._plan_step)
        
        backpoint_found, backpoint = self.find_backpoint(fpath.path, fpath.start_point)
        backpoint_index = fpath.path.index(backpoint) if backpoint_found else 0

        correct_index = max(backpoint_index - 3, 0)

        # poses = self.orient_path(fpath.path[(correct_index):])
        poses = self.orient_path(fpath.path)
        poses_stamped = [
            PoseStamped(
                pose=pose,
                header=Header(
                    stamp=rospy.Time.now()
                )
            )
            for pose in poses
        ]
        return Path(poses=poses_stamped)


    def prepare_to_publish_list(self, points : List[Point]) -> Path:
        spath = SPath(points=points)
        # spath.smooth(0.02, step=self._plan_step)

        poses = self.orient_path(spath.path)
        poses_stamped = [
            PoseStamped(
                pose=pose,
                header=Header(
                    stamp=rospy.Time.now()
                )
            )
            for pose in poses
        ]
        return Path(poses=poses_stamped)


    def on_goal_reached(self):
        if self._fork_detected_state:
            print('GOAL REACHED MESSAGE')
            self._turned_state = True
        else:
            pass


    def sort_fork(self, fpath : FPath):
        some_fork_step = my_math.substract(fpath.fork_steps[0], fpath.last_step)
        fpath.fork_steps.sort(key=lambda step : my_math.angle_between_vectors2D(
                    some_fork_step, my_math.substract(step, fpath.last_step)
                )
            )
        return fpath.fork_steps


    def fork_buf_push(self, buf_record : List[Point]):
        self._fork_buf.append(buf_record)
        if len(self._fork_buf) > self.FORK_BUF_LEN:
            self._fork_buf.pop(0)
        

    def get_plan(self) -> Path:
        return self._plan


    def turn_correct_side(self, path : List[Pose]) -> List[Pose]:
        distance_to_begin = my_math.sqr_length(path[0].position)
        distance_to_end = my_math.sqr_length(path[len(path) - 1].position)
        if distance_to_end < distance_to_begin:
            path.reverse()
        return path


    def insert_sequence(self, path : List[Point], sequence : List[Point]) -> List[Point]:
        print('insert: new sequence len:', len(sequence))
        # assuming the first point (if exists) in path is start point
        path_copy = path

        _, begin_backpoint = self.find_backpoint(path_copy, sequence[0])
        _, end_backpoint = self.find_backpoint(path_copy, sequence[-1])
        begin_backindex = path_copy.index(begin_backpoint)
        end_backindex = path_copy.index(end_backpoint)
        
        if (begin_backindex == end_backindex and
                end_backindex == len(path) - 1):
            path_copy = self.insert_end(path_copy, sequence, begin_backindex)
            return path_copy
        # <
        path_copy = self.insert_mid(path_copy, sequence, begin_backindex, end_backindex)
        return path_copy


    def insert_end(self, path : List[Point], sequence : List[Point],
            insert_index : int) -> List[Point]:
        # Check for sharp angle
        if insert_index == 0:
            
            return [path[0]] + sequence
        for i in reversed(range(insert_index)):
            if math.fabs(my_math.angle_3points(path[i], path[i+1], sequence[0])) < math.pi / 3:
                continue
            if (len(sequence) > 1 and
                math.fabs(my_math.angle_3points(path[i+1],
                          sequence[0], sequence[1])) < math.pi / 3):
                continue
            return path[:(i+1)] + sequence
        
        print(f'insert_end failed')
        return path


    def insert_replace(self, path : List[Point],
            sequence : List[Point]) -> List[Point]:
        path_copy = path
        _, backpoint = self.find_backpoint(path_copy, sequence[0])
        backindex = path_copy.index(backpoint)
        return self.insert_end(path, sequence, backindex)


    def insert_mid(self, path : List[Point], sequence : List[Point], 
            begin_index : int, end_index : int) -> List[Point]:
        seq_len = len(sequence)
        path_len = len(path)

        attach_end = False
        for i in range(end_index, path_len):
            if (seq_len > 1 and
                    math.fabs(my_math.angle_3points(sequence[-2], sequence[-1], path[i])) < math.pi / 3 or
                    seq_len == 1 and my_math.angle_3points(path[begin_index], sequence[-1], path[i]) < math.pi / 3):
                continue
            if (i + 1 < path_len and 
                    math.fabs(my_math.angle_3points(sequence[-1], path[i], path[i+1])) < math.pi / 3):
                continue
            attach_end = True # angles resolved
            break
        
        end_index = i if attach_end else path_len

        # Starting from 1, not changing the first point 
        for i in reversed(range(1, begin_index)):
            if (seq_len > 1 and 
                    math.fabs(my_math.angle_3points(path[i], sequence[0], sequence[1])) < math.pi / 3 or
                seq_len == 1 and attach_end and
                    my_math.angle_3points(path[i], sequence[0], path[end_index]) < math.pi / 3):
                continue
            if (i > 0 and 
                    math.fabs(my_math.angle_3points(path[i-1], path[i], sequence[0])) < math.pi / 3):
                continue
            return path[:(i + 2)] + sequence + path[end_index:]

        # angles not resolved
        self._debug and self._debug.add_full_path(path)
        self._debug and self._debug.add_new_path(sequence)
        self._debug.push()
        print('insert failed')
        return path

    # na pomoyku
    def find_backpoint(self, path : List[Point], init_point : Point) -> Tuple[bool, Point]:
        if len(path) <= 1:
            return True, path[0]
        # backposes and distances to projections
        projections : List[Tuple[int, float]] = []
        for i in range(len(path) - 1):
            projection = my_math.project_on_line(init_point, path[i], path[i+1])

            if my_math.points_on_different_sides(projection, path[i], path[i+1]):
                distance = my_math.distance2D(projection, init_point)
                projections.append((i, distance))
        
        distances_to_points = [my_math.distance2D(init_point, point)
                               for point in path]
        index_of_nearest_point = min(enumerate(distances_to_points), key=itemgetter(1))[0]
        nearest_point = path[index_of_nearest_point]

        if len(projections) > 0:
            index_of_nearest_projection = min(projections, key = itemgetter(1))[0]
            nearest_projection = path[index_of_nearest_projection]
            if (my_math.distance2D(nearest_projection, init_point) <
                my_math.distance2D(nearest_point, init_point)):
                return True, nearest_projection
            else:
                try:
                    point_before = path[index_of_nearest_point - 1]
                    point_after = path[index_of_nearest_point + 1]
                except IndexError:
                    return True, nearest_point
                
                distance_to_before = my_math.distance2D(init_point, point_before)
                distance_to_after = my_math.distance2D(init_point, point_after)
                if distance_to_before <= distance_to_after:
                    backpoint = point_before
                else:
                    backpoint = nearest_point
                return True, backpoint
        else:
            return True, nearest_point


    def find_backpoint(self, points : List[Point], init_point : Point):
        nearest_point = min(
            points,
            key=lambda point : (init_point - point).magnitude2D()
        )
        return True, nearest_point


    # def find_backpoint(self, path : List[Pose], init_point : Point) -> Tuple[bool, Point]:
    #     res, pose = self.find_backpose(path, init_point)
    #     return res, pose.position


    def average_path_step(self, path : List[Pose]) -> float:
        if len(path) <= 1:
            return 0
        distances : List[float] = []
        for i in range(len(path) - 1):
            distance = my_math.distance2D(path[i].position, path[i-1].position)
            distances.append(distance)
        return np.average(distances)/(len(path) - 1)


    def orient_path(self, path : List[Point]) -> List[Pose]:
        new_path = [Pose(position=point) for point in path]
        if len(path) <= 1:
            return new_path
        for i in range(1, len(new_path)):
            segment = my_math.substract(new_path[i].position, new_path[i-1].position)
            segment_orientation = my_math.vector_quaternion2D(segment)
            new_path[i].orientation = segment_orientation
        new_path[0].orientation = new_path[1].orientation
        return new_path
    

    def equidistant_transform(self, path : List[Point], distance : float,
            last_step : Point) -> List[Point]:
        if len(path) <= 1 or distance == 0:
            return path
        equid_path : List[Point] = []
        cur_point = path[0]
        path_index = 1
        while True:
            intersection = None
            for j in range(path_index , len(path)):
                if my_math.distance2D(cur_point, path[j]) < distance:
                    continue
                intersection = my_math.circle_section_intersection(cur_point, distance,
                                                                    path[j-1], path[j])
                path_index = j
                break
            equid_path.append(cur_point)
            if intersection is None:
                break
            cur_point = intersection
        if my_math.distance2D(equid_path[len(equid_path) - 1], last_step) > distance:
            last_intersection = my_math.circle_section_intersection(
                equid_path[len(equid_path) - 1], distance,
                equid_path[len(equid_path) - 1], last_step
                )
            equid_path.append(last_intersection)
        return equid_path


    def build_obstacle_avoiding_path(self):
        # enter here only after smoothing path
        if not self._cur_obstacle.contains(self._main_path[-1]):
            return self._main_path
        # find the enter point
        enter_point = None
        for point in reversed(self._main_path):
            if not self._cur_obstacle.contains(point):
                enter_point = point
                break
        if not enter_point:
            raise Exception('PATH HANDLER: The whole self._main_path lies in an obstacle, aborting')
        direction = (self._main_path[-1] - self._main_path[-2]).normalize2D()
        direction_back = -direction
        
        step_forward = self._cur_obstacle.push_forward_path(self._main_path)
        step_back = self._cur_obstacle.push_back_path(self._main_path)
            
        forward = ((self._main_path[-1] - step_forward[-1]).magnitude2D()
                 < (self._main_path[-1] - step_back[-1]).magnitude2D())
        print('DEBUG avoided')
        if forward:
            points = step_forward
        else:
            points = step_back
        return self.prepare_to_publish_list(points)
    

    def publish_fpath_debug(self, fpath : FPath):
        path, *forks = [
            [
                PoseStamped(
                    pose=Pose(
                        position=point
                    ),
                    header=Header(
                        stamp=rospy.Time.now()
                    )
                )
                for point in points
            ]
            for points in [fpath.path, *(fpath.forks)]
        ]
        self._new_segment_pub.publish(
            Path(
                header=Header(frame_id='map'),
                poses=path
            )
        )
        if len(forks) >= 2:
            self._new_fork_pub1.publish(
                Path(
                    header=Header(frame_id=f'map'),
                    poses=forks[0]
                )
            )
            self._new_fork_pub2.publish(
                Path(
                    header=Header(frame_id=f'map'),
                    poses=forks[1]
                )
            )
    

    def publish_prev_fpath_debug(self, fpath : FPath):
        if fpath is None:
            return
        path, *forks = [
            [
                PoseStamped(
                    pose=Pose(
                        position=point
                    ),
                    header=Header(
                        stamp=rospy.Time.now()
                    )
                )
                for point in points
            ]
            for points in [fpath.path, *(fpath.forks)]
        ]
        self._new_segment_pubp.publish(
            Path(
                header=Header(frame_id='map'),
                poses=path
            )
        )
        self._new_fork_pub1p.publish(
            Path(
                header=Header(frame_id=f'map'),
                poses=forks[0]
            )
        )
        self._new_fork_pub2p.publish(
            Path(
                header=Header(frame_id=f'map'),
                poses=forks[1]
            )
        )


    def publish_origin_debug(self, origin : Ray, robot_pose : Pose):
        q = my_math.quaternion_from_euler(0, 0, origin.direction)
        ror = robot_pose.orientation
        r_q = [ror.x, ror.y, ror.z, ror.w]
        q = my_math.quaternion_multiply(r_q, q)
        orientation = Quaternion(x=q[0], y=q[1], z=q[2], w=q[3])
        origin_point = my_math.rotate_2d_point_gmsgs(origin.origin, ror)
        origin_point = robot_pose.position + origin_point
        self._origin_pub.publish(
            Marker(
                header=Header(frame_id=f'map'),
                ns='origin', id=0, type=Marker.ARROW,
                pose=Pose(
                    position=origin_point,
                    orientation=orientation
                ),
                scale=Vector3(x=0.5,y=0.1,z=0.1),
                color=ColorRGBA(r=0,g=1,b=1,a=1),
                lifetime=rospy.Duration(1)
            )
        )

    
    def direction_publisher_debug(self, _):
        if self._vector_forward_debug is None:
            return
        self._debug_marker1_pub.publish(
            Marker(
                header=Header(frame_id=f'map'),
                ns='origin', id=0, type=Marker.SPHERE,
                pose=Pose(
                    position=self._vector_forward_debug,
                ),
                scale=Vector3(x=0.1, y=0.1, z=0.1),
                color=ColorRGBA(r=1, g=1, b=1, a=1),
                lifetime=rospy.Duration(1)
            )
        )


    def publish_marker1_debug(self, point : Point):
        if point is None:
            return
        self._debug_marker1_pub.publish(
            Marker(
                header=Header(frame_id=f'map'),
                ns='origin', id=0, type=Marker.SPHERE,
                pose=Pose(
                    position=point,
                ),
                scale=Vector3(x=0.1, y=0.1, z=0.1),
                color=ColorRGBA(r=1, g=1, b=1, a=1),
                lifetime=rospy.Duration(1)
            )
        )


    def publish_state_debug(self, _):
        message = PathFinderState(
            state=self._state.id,
            turn_index=self._turn_index if self._turn_index is not None else 0,
            turns_total=2
        )
        self._debug_state_pub.publish(message)
    

    def reset_stored_plan(self):
        self._main_path = SPathMain(
            step=self.INIT_PLAN_STEP,
            smooth_scale=self.SMOOTH_SCALE,
            turn_step=self.TURN_STEP,
            tolerance=4*self.POINT_TOLERANCE
        )
        self._plan = Path()


    def direction_deviation_angle(self, robot_pose : Point):
        vector_to_point = self._direction_point - robot_pose.position
        self._vector_forward_debug = vector_forward = my_math.vector_towards_quaternion(robot_pose.orientation)
        return my_math.angle_between_vectors2D(vector_forward, vector_to_point)


    def direction_maintainer(self, _):
        # _direction_point is in map coordinate system
        if self._state.directing:
            return
        if not self._direction_point:
            return
        robot_pose = self.get_robot_pose()
        distance = (robot_pose.position - self._direction_point).magnitude2D()
        if distance < self.PATH_INNER_RADIUS * 1.3:
            print('DEBUG less than inner radius')
            self._direction_point = None
            return
        angle = self.direction_deviation_angle(robot_pose)
        print('DEBUG angle', 180 * angle / 3.14 )
        if (math.fabs(angle) < (self.VIEWING_ANGLE / 2)):
            print('DEBUG in viewing field')
            return
        if self._state.fork_callback:
            print('DEBUG Do not see, wait for fork')
            return
        print('DEBUG Начинаем доворачивать')
        self._searching_command = SearchCommandSequence([int(angle/math.fabs(angle))], 2)
        self._searching_command.next(
                robot_pose
            )
        self._state = StateMachine.DIRECTING(self._state)