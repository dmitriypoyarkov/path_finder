import math
from tracemalloc import start
from typing import List, Tuple, Union, overload

import numpy as np
from geometry_msgs.msg import Point, Quaternion
from graph_analyzer.msg import PathFinderGoal
from scipy import interpolate

import my_math
import rostools
from cell import Cell
from spath import SPath


class FPath(object):
    @overload
    def __init__(self, path_points : List[Point], **kwargs): ...


    @overload
    def __init__(self, path_point: Point, **kwargs): ...


    @overload
    def __init__(self, path_cells : List[Cell],
            fork1 : List[Cell], fork2 : List[Cell], **kwargs): ...

    @overload
    def __init__(self, path_cells : List[Cell],
            **kwargs): ...


    def __init__(self, **kwargs):
        self._start_point : Point = kwargs.get('start_point')
        self._fork_steps = []

        path_cells : List[Cell] = kwargs.get('path_cells')
        self._fork1 : List[Cell] = kwargs.get('fork1', [])
        self._fork2 : List[Cell] = kwargs.get('fork2', [])
        self._forks = []


        self._step_length = 0.1
        self._path_with_fork = []
        self._fork_attached = False
        path_points : List[Point] = kwargs.get('path_points')

        if path_cells is not None:
            self._path = SPath([cell.center for cell in path_cells])
            self._path_with_fork = self._path
            if len(self._fork1) > 0:
                self._forks = [SPath([cell.center for cell in self._fork1]),
                    SPath([cell.center for cell in self._fork2])]
                self._fork_steps = [fork[0] for fork in self._forks]
            return

        if isinstance(path_points, list):
            self._path = SPath(path_points)
            return

        if isinstance(path_points, Point):
            self._path = SPath([path_points])
            return

        print('FPATH: Invalid constructor arguments')


    @property
    def step_length(self):
        return self._step_length
    
    
    @step_length.setter
    def step_length(self, value):
        self._step_length = value


    @property
    def fork_steps(self):
        return [fork[0] for fork in self._forks]


    @property
    def path(self):
        return self._path


    @path.setter
    def path(self, value : SPath):
        if isinstance(value, list):
            raise Exception('попался')
        self._path = value
    
    
    @property
    def last_step(self):
        if len(self._path) == 0:
            return None
        return self._path[-1]


    @property
    def start_point(self):
        return self._start_point
    
    @start_point.setter
    def start_point(self, value):
        self._start_point = value


    @property
    def max_distance(self):
        if len(self._path) == 0:
            return 0
        return max([my_math.distance2D(self._start_point, point)
            for point in self._path])


    @property
    def forks(self):
        return self._forks


    @property
    def has_fork(self):
        return len(self._forks) > 0


    def begin_distance(self, origin : Point):
        if len(self.path) == 0:
            return 0.0
        distance = (origin - self.path[0]).magnitude2D()
        return distance


    def end_distance(self, origin : Point):
        if len(self.path) == 0:
            return 0.0
        ends = ([self.path[-1]] +
            [fork[-1] for fork in self.forks])
        return max([(end - origin).magnitude2D()
            for end in ends])


    def select_path_before_fork(self, graph : list) -> Tuple[List[tuple], List[tuple]]:
        path : List[tuple] = []
        next_steps : List[tuple] = []
        for i, item in enumerate(graph):
            if not isinstance(item, tuple):
                remaining_lists = graph[i:]
                next_steps = [item[0] for item in remaining_lists]
                break
            path.append(item)
        return path, next_steps


    def calculate_length(self):
        if len(self._path) <= 1:
            return 0.0
        length = 0.0
        for i in range(1, len(self._path)):
            length += (self._path[i] - self._path[i - 1]).magnitude2D()
        return length


    def parse_cells_mat(self, cells_mat : List[List[Cell]],
        indices : List[Tuple[int, int]]) -> List[Point]:
        
        selected_cells = [cells_mat[index[0]][index[1]] for index in indices]
        points = [cell.shifted_point for cell in selected_cells]
        return points


    def get_nonlooped_length(self, path):
        length = 0
        for index in path:
            if isinstance(index, tuple):
                length += 1
                continue
            break
        return length
    

    @overload
    def rotate(self, orientation : Quaternion): ...
    @overload
    def rotate(self, yaw : float): ...

    def rotate(self, rotation : Union[Quaternion, float]):
        self._path.rotate(rotation)
        for fork in self.forks:
            fork.rotate(rotation)


    def shift_path(self, shift_amount : Point):
        self.path.shift(shift_amount)
        for fork in self._forks:
            fork.shift(shift_amount)
        # self._fork1 = [my_math.add(point, shift_amount)
        #              for point in self._fork1]
        # self._fork2 = [my_math.add(point, shift_amount)
        #              for point in self._fork2]


    def scale_path(self, factor : float):
        self.path = [my_math.multiply(point, factor)
                     for point in self._path]
        self._fork_steps = [my_math.multiply(point, factor)
                     for point in self._fork_steps]


    def get_path_count(self):
        return len(self._path)


    def get_length(self):
        return self.calculate_length()


    def get_fitness(self):
        # return self.average_path_density
        # return self.average_density
        return self.calculate_length()
        return len(self._path) * self.calculate_length()
        # return (len(self._path)**0.5) * self.calculate_length()


    def get_path(self):
        return self._path


    def get_next_steps(self):
        return self._fork_steps


    def smooth(self, scale=0.2, step=0.1):
        self._path.smooth(scale, step)
        for fork in self.forks:
            fork.smooth(scale, step)
        # for fork in self._forks:
        #     fork.smooth(step, scale)
        # length = self.calculate_length()
        # if length <= step * 4:
        #     return
        # while len(self._path) <= 4:
        #     self._path = self._path.mill
        # weights = np.ones(len(self._path))
        # weights[0] = 5
        # weights[-1] = 5
        # tck, u = interpolate.splprep([[pt.x for pt in self._path],
        #     [pt.y for pt in self._path]], s=scale, w=weights)
        # nodes_count = math.ceil(length / step)
        # result = interpolate.splev(np.linspace(0, 1, nodes_count), tck)
        # self.path = [Point(x=x, y=y) for x, y in zip(result[0], result[1])]
    

    def clear(self):
        self._path.clear()
        self._fork1.clear()
        self._fork2.clear()
        self._fork_steps.clear()


    # def sort_forks(self, forks):
    #     some_fork_step = my_math.substract(forks[0][-1], self.last_step)
    #     forks.sort(key=lambda fork : my_math.angle_between_vectors2D(
    #                 some_fork_step, my_math.substract(fork[-1], self.last_step)
    #             )
    #         )
    #     return forks
    

    def sort_forks(self):
        some_fork_step = my_math.substract(self.forks[0][0], self.last_step)
        self.forks.sort(key=lambda fork : my_math.angle_between_vectors2D(
                    some_fork_step, my_math.substract(fork[0], self.last_step)
                )
            )


    def cut_path(self, path : List[Point], length : float):
        for i in range(len(path)):
            if my_math.path_len(path[:i]) >= length:
                return path[:i]
        return path


    def with_fork(self, index : int, step_length : float):
        self.sort_forks()
        attachment = self.cut_path(self.forks[index], step_length)
        result = self.path + self.forks[index]
        # result.smooth()
        return result


    def fork_diff(self, other_fpath : "FPath"):
        distance_sum = 0.0
        total_points = 0
        self.sort_forks()
        other_fpath.sort_forks()
        for i in range(2):
            forks_to_compare = [self.forks[i], other_fpath.forks[i]]
            forks_to_compare.sort(key=my_math.path_len)
            for point in forks_to_compare[0]:
                distance = min([(point - other).magnitude2D()
                    for other in forks_to_compare[1]])
                distance_sum += distance
            total_points += len(forks_to_compare[0])
        return distance_sum / total_points
    

    def continues_old(self, other_path : "SPath", tolerance : float,
            required_percent=0.25):
        if len(self.path) == 0:
            return False
        if len(other_path) == 0:
            return True
        distances = [
            min([(point - other_point).magnitude2D()
                    for other_point in other_path])
                for point in self.path
        ]
        near_count = len([value for value in distances
            if value < tolerance])
        near_percent = near_count / len(distances)
        return near_percent > required_percent


    def continues(self, other_path : "SPath", tolerance : float,
            required_percent=0.225):
        if len(self.path) == 0:
            return False
        if len(other_path) == 0:
            return True
        dist_from_other = [
            min([(other_point - point).magnitude2D()
                    for point in self.path])
                for other_point in other_path
        ]
        distances_to_start = [
            (other_point - self.path[0]).magnitude2D()
            for other_point in other_path
        ]
        # i_varname stands for tuple (index, variable) from enumerate
        i_first_contact = min(
            enumerate(distances_to_start),
            key=lambda i_distance : i_distance[1]
        )
        if i_first_contact[1] < tolerance:
            return False
        first_contact_index = i_first_contact[0]
        distances = [
            min([(other_point - self_point).magnitude2D()
            for self_point in self.path])
            for other_point in other_fpath
        ]
        contacts = [distance < tolerance
            for distance in distances]

        try:
            contacts.index(False)
        except ValueError:
            contact_len = (len(other_path) - 1 - first_contact_index)
            if contact_len > required_percent * len(self.path):
                return True
        return False