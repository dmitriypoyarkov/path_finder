from typing import List
import numpy as np

from debug_manager import DebugManager
from fpath import FPath
from geometry_msgs.msg import Point
from rect import Rect
from sensor_msgs.msg import CameraInfo

from bokeh.models import (Button, CustomJS, ColumnDataSource, Grid,
                          HexTile, LinearAxis, Plot, Slider, Text)
from bokeh.layouts import column
from bokeh.plotting import figure, output_file, show, curdoc
from bokeh.core.enums import ButtonType

from cell import Cell
from sfag import SFAG

from bokeh.util.hex import cartesian_to_axial


def on_num_change(attr, old, new):
    global num, source_circles
    num = new
    if data_file[num].get('fpath'):
        sos = data_file[num]['fpath']['source']
        source_circles.data_source.data = {
            'x' : [item.x for item in sos],
            'y' : [item.y for item in sos]
        }


def on_scale_change(attr, old, new):
    global fig, split_scale
    split_scale = new
    # sos = data_file[num]['fpath']['source']
    # split = sfag.min_area_split(new, sos)
    # fig.title.text = f"area: {split['area']}"
    # put_cells(split['cells'])


def on_put_full_click():
    global num, source_circles,\
        new_path_line, full_path_line
    new = data_file[num]['new_path']
    full = data_file[num]['full_path']
    new_path_line.data_source.data = {
        'x' : [item.x for item in new],
        'y' : [item.y for item in new]
    }
    full_path_line.data_source.data = {
        'x' : [item.x for item in full],
        'y' : [item.y for item in full]
    }


def on_buildgraph_click():
    global sfag, graphs_lines, global_cells, fig
    print([cell.center for cell in global_cells])
    paths = sfag.build_graph(global_cells, debug=True)
    # paths = sfag.build_graph(global_cells, debug=False)
    # print('paths', paths.has_fork())
    if not isinstance(paths, FPath):
        fig.title.text = f'paths {len(paths)}'
        for i, path in enumerate(paths):
            graphs_lines[i].data_source.data = {
                'y' : [cell.center.x for cell in path],
                'x' : [cell.center.y for cell in path]
            }
        i += 1
    else:
        i = 0
    while i < len(graphs_lines):
        graphs_lines[i].data_source.data = {
            'y' : [],
            'x' : []
        }
        i += 1


def on_plotareas_click():
    global sfag, areas_line, split_scale
    points = data_file[num]['fpath']['source']

    scales = list(range(2, 40))
    areas = []
    section = sfag.max_connected_section(points, 0.2, split_scale)
    put_cells(section['cells'])
    ############
    # split = sfag.autosplit(points)
    ############
    # for scale in scales:
    #     section = sfag.max_connected_section(points, 0.2, scale)
    #     areas.append(section['area'])
    
    points = [point for point in points if sfag._rect.contains(point)]
    min_corner = Point(
        x=min([point.x for point in points]),
        y=min([point.y for point in points])
    )
    max_corner = Point(
        x=max([point.x for point in points]),
        y=max([point.y for point in points])
    )
    # section = sfag.max_connected_section(points, 0.3, split_scale)
    # put_cells(section['cells'])

    # section = sfag.distribute(split_scale, points,
    #     min_corner, max_corner, shift_x, shift_y)
    # put_cells([cell for row in section for cell in row])

    # areas_line.data_source.data = {
    #     'x' : scales,
    #     'y' :  areas
    # }


def put_cells(cells : List[Cell]):
    global fig, hex, global_cells
    hex and fig.renderers.remove(hex)
    new_source = ColumnDataSource({
        'x' : [cell.center.x for cell in cells],
        'y' : [cell.center.y for cell in cells]
    })
    hex = fig.hex('y', 'x', source=new_source, size=120, fill_alpha=0)
    global_cells = cells


def on_filter_click():
    global hex, text, fork_line, histogram

    sos = data_file[num]['fpath']['source']
    fpath = sfag.filter_new(sos)
    some_cell : Cell = fpath.cells_mat[0][0]

    # glyph_data = glyph_rend.data_source.data
    hex and fig.renderers.remove(hex)
    cells_list = [cell for row in fpath.cells_mat for cell in row]
    cartesian_x = [cell.center.x for cell in cells_list]
    cartesian_y = [cell.center.y for cell in cells_list]
    new_source = ColumnDataSource({
        'y' : cartesian_y,
        'x' : cartesian_x
    })

    hex = fig.hex('y', 'x', source=new_source, size=120, fill_alpha=0)

    text.data_source.data = {
        'x' : cartesian_x,
        'y' : cartesian_y,
        'text' : [f'{cell.density():.1f}' if cell.density() > 0 else '' for cell in cells_list]
    }

    fork_line.data_source.data = {
        'x' : [step.x for step in fpath.fork_steps],
        'y' : [step.y for step in fpath.fork_steps]
    }

    cur_data = filtered_line.data_source.data
    new_data = {**cur_data}
    new_data['filtered_x'] = [point.x for point in fpath.path]
    new_data['filtered_y'] = [point.y for point in fpath.path]
    filtered_line.data_source.data = new_data
    print('density: ', fpath.average_density, 'distr', fpath._distribution_scale)
    # histogram and fig.renderers.remove(histogram)
    # hist, ye, xe = fpath.histogram
    # histogram = fig.image(image=[hist], x=xe[0], y=ye[0], dw=xe[-1] - xe[0], dh=ye[-1] - ye[0], palette="Spectral11")


def on_filter_instance_click():
    global data_file, hex, text, fork_line

    sos = data_file[num]['fpath']['source']
    fpath = sfag.filter_instance(sos, distribution, shift_x, shift_y)
    some_cell : Cell = fpath.cells_mat[0][0]

    # glyph_data = glyph_rend.data_source.data
    hex and fig.renderers.remove(hex)
    cells_list = [cell for row in fpath.cells_mat for cell in row]
    cartesian_x = [cell.center.x for cell in cells_list]
    cartesian_y = [cell.center.y for cell in cells_list]
    new_source = ColumnDataSource({
        'y' : cartesian_y,
        'x' : cartesian_x
    })

    hex = fig.hex('y', 'x', source=new_source, size=120, fill_alpha=0)

    text.data_source.data = {
        'x' : cartesian_x,
        'y' : cartesian_y,
        'text' : [f'{cell.density():.1f}' if cell.density() > 0 else '' for cell in cells_list]
    }

    fork_line.data_source.data = {
        'x' : [step.x for step in fpath.fork_steps],
        'y' : [step.y for step in fpath.fork_steps]
    }

    cur_data = filtered_line.data_source.data
    new_data = {**cur_data}
    new_data['filtered_x'] = [point.x for point in fpath.path]
    new_data['filtered_y'] = [point.y for point in fpath.path]
    filtered_line.data_source.data = new_data

    print('density: ', fpath.average_density)


def on_distrib_change(attr, old, new):
    global distribution
    distribution = new
    on_filter_instance_click()


def on_shift_x_change(attr, old, new):
    global shift_x
    shift_x = new


def on_shift_y_change(attr, old, new):
    global shift_y
    shift_y = new


num = 0
fig = figure(match_aspect = True)
debug = DebugManager(data_file='points.json', cam_info_file='cam_info.json',
    rect_file='rect.json')
data_file = debug.load_data()
rect = debug.load_rect('/yarp8r1/sensors/camera/image_raw')
sfag = SFAG(rect)

split_scale = 1
global_cells = []

if data_file[num].get('fpath'):
    item0 = data_file[num]['fpath']['source']
    source = {
        'x' : [p.x for p in item0],
        'y' : [p.y for p in item0]
    }

f_source = {
    'filtered_x' : [],
    'filtered_y' : []
}

hex_source = ColumnDataSource({
    'q' : [],
    'r' : []
})

if data_file[num].get('fpath'):
    item_forks = data_file[num]['fpath']['forks']
    fork_source = ColumnDataSource({
        'x' : [item.x for item in item_forks],
        'y' : [item.y for item in item_forks]
    })
    fork_havers = [i for i in range(len(data_file)) if len(data_file[i]['fpath']['forks']) > 0]
    print('fffffffff: ', data_file[0]['fpath']['source'])

hex = None
# glyph = HexTile(q="q", r="r", size=5, fill_color="#fb9a99", line_color="white")
# glyph_rend = fig.add_glyph(hex_source, glyph)
# plot.add_glyph(source, glyph)
if data_file[num].get('fpath'):
    source_circles = fig.circle('y', 'x', size=5, source=source, color="navy")
    fork_line = fig.circle('y', 'x', size=5, source=fork_source, color="green")

    item0_path = data_file[num]['fpath']['path']

# x = [p.x for p in item0_path]
# y = [p.y for p in item0_path]
# fig.circle(y, x, size=5, color="red", alpha=0.5)

    item0_forks = data_file[num]['fpath']['forks']
filtered_line = fig.line('filtered_y', 'filtered_x', source=f_source, color="red")

text = fig.text('y', 'x', 'text', text_font_size="26px",
           text_baseline="middle", text_align="center")

distribution = 1
shift_y = 0
shift_x = 0
distrib_slider = Slider(start=1, end=11, value=distribution, step=1, title='Distribution')
shift_y_slider = Slider(start=0, end=1, value=shift_y, step=.1, title='ShiftY')
shift_x_slider = Slider(start=0, end=1, value=shift_x, step=.1, title='ShiftX')
distrib_slider.on_change('value', on_distrib_change)
shift_x_slider.on_change('value', on_shift_x_change)
shift_y_slider.on_change('value', on_shift_y_change)

num_slider = Slider(start=0, end=len(data_file) - 1, value=0, step=1, title="Num")
num_slider.on_change('value', on_num_change)
filter_button = Button(label="filter", button_type="success")
filter_button.on_click(on_filter_click)
filter_inst_button = Button(label="Filter instance", button_type="success")
filter_inst_button.on_click(on_filter_instance_click)

scale_slider = Slider(start=0, end=40, value=0, step=1, title="Scale")
scale_slider.on_change('value', on_scale_change)

histogram = None

plot_fig = figure(match_aspect=True)
areas_line = plot_fig.line('x', 'y', source=ColumnDataSource({'x' : [], 'y' : []}))

plotareas_button = Button(label='areasplot', button_type="success")
plotareas_button.on_click(on_plotareas_click)

graphs_lines = [fig.line('x', 'y', source = ColumnDataSource({'x' : [], 'y' : []})) for _ in range(30)]

buildgraph_button = Button(label='build', button_type='success')
buildgraph_button.on_click(on_buildgraph_click)


new_path_line = fig.line('y', 'x', source = ColumnDataSource({'x' : [], 'y' : []}), color='red')
full_path_line = fig.line('y', 'x', source = ColumnDataSource({'x' : [], 'y' : []}))
put_full_button = Button(label='full', button_type='success')
put_full_button.on_click(on_put_full_click)

curdoc().add_root(column(fig, put_full_button, filter_button, plotareas_button, buildgraph_button, num_slider,
    distrib_slider, shift_x_slider, shift_y_slider, filter_inst_button,
    scale_slider))