#!/usr/bin/env python3
# region imports
import os
from typing import List

import rospy
from cv_bridge import CvBridge
from geometry_msgs.msg import Point
from rospy.topics import Publisher
from sensor_msgs.msg import CameraInfo
from std_msgs.msg import ColorRGBA
from tf import TransformListener

from debug_manager import DebugManager
from path_detector import PFPathDetector
from path_handler import PathHandler

# endregion

ROBOT_FRAME = 'map'
ROBOT_BASE_LINK = 'base_link'

CAM_INFO_TOPIC = '/yarp8r2/sensors/camera/camera_info'
SERVICE_NAME = 'global_planner_service'

RED_COLOR = ColorRGBA(r=1, g=0, b=0, a=1)
BLUE_COLOR = ColorRGBA(r=0, g=0, b=1, a=1)


def main():
    rospy.init_node('global_planner_service_node', log_level=rospy.DEBUG)
    rospy.logdebug('GLOBAL PLANNER NODE: inited')
    cam_info = rospy.wait_for_message(CAM_INFO_TOPIC, CameraInfo)
    rospy.logdebug('GLOBAL PLANNER NODE: Camera info get')

    module_path = os.path.dirname(os.path.abspath(__file__))
    debug = DebugManager(
        data_file=os.path.join(module_path, 'points.json'),
        cam_info_file=os.path.join(module_path, 'cam_info.json'),
        rect_file=os.path.join(module_path, 'rect.json')
    )
    debug.save_cam_info(cam_info, CAM_INFO_TOPIC)

    path_detector = PFPathDetector(
        rospy.get_param('image_raw_topic'),
        cam_info,
        'map',
        rospy.get_param('base_link_frame'),
        0.16, [-0.02, 0, 1, 0],
        debug=debug
    )

    path_handler = PathHandler(path_detector,
        rospy.get_param('move_base_topic'),
        debug=debug
    )
    rospy.logdebug('GLOBAL PLANNER: Ready')
    rospy.spin()
    return


def main_with_performance_test():
    import yappi
    yappi.start()

    main()
    yappi.stop()
    threads = yappi.get_thread_stats()
    with open('output.prof', 'w') as file:
        for thread in threads:
            print(
                "Function stats for (%s) (%d)" % (thread.name, thread.id),
                file=file
            )  # it is the Thread.__class__.__name__
            stats = yappi.get_func_stats(ctx_id=thread.id)
            stats.sort('tsub')
            stats.print_all(file)
            print('\n', file=file)


if __name__ == "__main__":
    main()
