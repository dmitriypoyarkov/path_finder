from typing import List

from geometry_msgs.msg import Point
from shapely.geometry import Polygon
import shapely.geometry as sh

import rostools


class Obstacle(object):
    def __init__(self, position : Point,
        radius : float):
        self._position = position
        self._radius = radius


    @property
    def position(self):
        return self._position
    
    @property
    def radius(self):
        return self._radius
    
    
    def contains(self, point : Point):
        distance = (self._position - point).magnitude2D()
        if distance > self._radius:
            return False
        return True


    def intersect(self, vec : Point, origin : Point):
        return rostools.vector_circle_intersection(vec, origin,
            self._position, self._radius)


class Multiobstacle(object):
    def __init__(self, squares : List[Polygon]):
        self._squares = squares
        self._step = 0.02


    @property
    def position(self):
        return self._position
    
    @property
    def radius(self):
        return self._radius
    
    
    def contains(self, point : Point):
        for figure in self._squares:
            if figure.contains(sh.Point(point.x, point.y)):
                return True
        return False
    
    
    def push_forward_path(self, path : List[Point]):
        if not self.contains(path[-1]):
            return path
        step = (path[-1] - path[-2]).normalize() * self._step
        increment = path[-1] + step
        while self.contains(increment):
            increment += step
        return increment


    def push_back_path(self, path : List[Point]):
        if not self.contains(path[-1]):
            return path
        for i in range(-2, -len(path), -1):
            if not self.contains(path[i]):
                return path[:(i+1)]
