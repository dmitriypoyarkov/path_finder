import copy
import math
from abc import abstractmethod
from typing import List, Tuple

import cv2 as cv
import dynamic_reconfigure.server as dynamic
import numpy as np
import rospy
from cv_bridge import CvBridge
from geometry_msgs.msg import Point, Pose, Quaternion, Vector3
from graph_analyzer.cfg import PathFinderConfig
from scipy.spatial.transform import Rotation
from sensor_msgs.msg import CameraInfo, Image
from std_msgs.msg import ColorRGBA, Header
from tf import TransformListener
from visualization_msgs.msg import Marker, MarkerArray

import my_math
import rostools
from debug_manager import DebugManager
from fpath import FPath
from image_transform import ImageToWorldProjector
from my_math import keypoints_to_arr
from ray import Ray
from rect import Rect
from sfag import SFAG
from spath import SPath
from time import time


class FPathDetector(object):
    @abstractmethod
    def receive_and_detect(self, segment_origin : Ray) -> FPath: ...

    @property
    @abstractmethod
    def field_of_view(self) -> Rect: ...

    @abstractmethod
    def get_origin(self, path : List[Point]) -> Ray: ...


class PFPathDetector(FPathDetector):
    def __init__(self, frame_topic : str, cam_info : CameraInfo,
            robot_frame : str, base_link_frame : str,
            robot_cam_height : float, robot_cam_orientation : list,
            blob_color_bounds = ([15, 10, 0], [70, 255, 255]),
            debug : DebugManager = None):

        self._tf_listener = TransformListener()
        self._frame_topic = frame_topic
        self._robot_frame = robot_frame
        self._base_link_frame = base_link_frame
        self._blob_color_bounds = blob_color_bounds
        self._bridge = CvBridge()
        self._blob_params = self.init_blob_params()
        dynamic.Server(PathFinderConfig, self.reconfig_callback)
        self._cv_blob_detector = cv.SimpleBlobDetector_create(self._blob_params)

        self._image_projector = ImageToWorldProjector(cam_info, robot_cam_height,
            robot_cam_orientation)

        self._path_scale = 19 # 24
        self._path_step = 0.7

        test_frame = rospy.wait_for_message(frame_topic, Image)
        self._stored_frame = test_frame
        img = self._bridge.imgmsg_to_cv2(test_frame, 'bgr8')
        rect=self.get_img_world_rect(img)
        self._cam_info = cam_info
        (h,w) = img.shape[:2]
        self._new_size = (h,w)
        self._distorted_cam_matrix = np.array([[*cam_info.K[:3]], [*cam_info.K[3:6]], [*cam_info.K[6:9]]])
        self._f = [self._distorted_cam_matrix[0][0], self._distorted_cam_matrix[1][1]]
        self._c = [self._distorted_cam_matrix[0][2], self._distorted_cam_matrix[1][2]]
        self._cam_matrix, _ = cv.getOptimalNewCameraMatrix(self._distorted_cam_matrix,
            cam_info.D, (w,h), 1, (w,h))
        self._sfag = SFAG(rect)
        print('dist', self._distorted_cam_matrix, 'undist', self._cam_matrix)
        
        self._rviz_pub_debug = rospy.Publisher('~rviz_pub', MarkerArray, queue_size=2)
        self._keypoint_img_pub_debug = rospy.Publisher('~keypoint_img', Image, queue_size=2)
        self._nearest_point_debug = None

        rospy.Timer(rospy.Duration(0.8), self.rviz_publisher_debug, oneshot=False)
        self._debug = debug
        if debug:
            debug.save_rect(rect, frame_topic)

        rospy.Subscriber(frame_topic, Image, callback = self.save_frame_callback, queue_size=2)


    @property
    def field_of_view(self) -> Rect:
        robot_position = self.get_robot_pose().position
        return Rect(
            self._sfag._rect.top_left + robot_position,
            self._sfag._rect.bottom_right + robot_position,
            self._sfag._rect.flipped
        )


    def init_blob_params(self):
        params = cv.SimpleBlobDetector_Params()
        params.filterByCircularity = True
        params.maxCircularity = 100000
        params.filterByArea = True
        params.maxArea = 100000
        params.filterByConvexity = True
        params.maxConvexity = 100000000
        params.filterByInertia = True
        params.maxInertiaRatio = 10000000
        return params


    def reconfig_callback(self, config: PathFinderConfig, level):        
        self._blob_params.blobColor = config.get("blobColor")
        self._blob_params.minThreshold = config.get("minThreshold")
        self._blob_params.maxThreshold = config.get("maxThreshold")
        self._blob_params.minArea = config.get("minArea")
        self._blob_params.minCircularity = config.get("minCircularity")
        self._blob_params.minConvexity = config.get("minConvexity")
        self._blob_params.minInertiaRatio = config.get("minInertiaRatio")
        self._blob_params.maxArea = config.get('maxArea')

        self._blob_color_bounds[0][0] = config.get("minHue")
        self._blob_color_bounds[1][0] = config.get("maxHue")
        self._blob_color_bounds[0][1] = config.get("minSaturation")
        self._blob_color_bounds[1][1] = config.get("maxSaturation")
        self._blob_color_bounds[0][2] = config.get("minValue")
        self._blob_color_bounds[1][2] = config.get("maxValue")

        self._cv_blob_detector = cv.SimpleBlobDetector_create(self._blob_params)
        rospy.logdebug('PATH DETECTOR: Reconfigure blob parameters')
        return config


    def get_img_world_rect(self, img : np.ndarray) -> Rect:
        height, width, _ = img.shape
        corners = list(map(self._image_projector.project,
                        [np.array([0, height*0.55]), np.array([width, height*0.55]), np.array([0, height])]))
        top_left = Point(
            x=corners[0][0],
            y=corners[1][1]
        )
        bottom_right = Point(
            x=corners[2][0],
            y=corners[0][1]
        )
        rect = Rect(top_left, bottom_right, flipped = True)
        return rect


    def harris_filter(self, frame : Image):
        img = self._bridge.imgmsg_to_cv2(frame, 'bgr8')

        undist_frame = cv.undistort(img, self._cam_matrix,
            self._cam_info.D, None, self._cam_matrix)
        
        gray = cv.cvtColor(undist_frame,cv.COLOR_BGR2GRAY)
        gray = np.float32(gray)
        dst = cv.cornerHarris(gray, 7, 3, 0.04)
        dst = cv.dilate(dst, None)
        dst_min = dst.min()
        dist = dst.max() - max(dst_min, 0)
        ui = np.uint8(((dst + math.fabs(dst_min)) / dist) * 255)
        return ui


    def detect_special_markers(self, img : np.ndarray):
        # img = self._bridge.imgmsg_to_cv2(frame, 'bgr8')


        # undist_frame = cv.undistort(img, self._cam_matrix,
        #     self._cam_info.D, None, self._cam_matrix)
        
        # scale = 2 # percent of original size
        # dim = (int(undist_frame.shape[1] / scale),
        #     int(undist_frame.shape[0] / scale))
        
        # resize image
        # undist_frame = cv.resize(undist_frame, dim, interpolation = cv.INTER_AREA)
        gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        gray = np.float32(gray)
        t = time()
        dst = cv.cornerHarris(gray, 11, 3, 0.04)
        # print('DEBUG corner time', time() - t)
        #result is dilated for marking the corners, not important
        # dst = cv.dilate(dst,None)
        kernel = np.ones((5, 5), np.uint8)
        # dst = cv.erode(dst, kernel)
        # dst = cv.erode(dst, kernel)
        # print('DEBUG dilation time', time() - t)
        thresh = np.zeros(dst.shape, np.uint8)
        thresh[np.where(dst>15000)] = 255
        contours, hierarchy = cv.findContours(image=thresh, mode=cv.RETR_TREE, method=cv.CHAIN_APPROX_NONE)
        # indices = np.where(dst>0.05*dst.max())
        # result = [(indices[1][i] * 2, indices[0][i] * 2)
        #     for i in range(len(indices[1]))
        # ]
        result = np.array([
            [int(np.average(contour[:, 0, 0])),
            int(np.average(contour[:, 0, 1]))]
            for contour in contours
        ], dtype=np.float32)
        # result_undist = cv.undistortPoints(result, self._cam_matrix, self._cam_info.D)
        # result_final = [point[0] * self._f + self._c for point in result]
        # result_final = [(int(point[0]), int(point[1])) for point in result_final]
        # print('DEBUG tuple time', time() - t)
        return result


    def receive_and_detect(self, segment_origin : Ray = Ray()) -> FPath:
        '''
        Receives frame from frame_topic, detects an FPath on it
        and returns it.
        '''
        rospy.logdebug("Got a frame, processing...")
        image = self._bridge.imgmsg_to_cv2(self._stored_frame, 'bgr8')
        undist_frame = cv.undistort(image, self._cam_matrix,
            self._cam_info.D, None, self._cam_matrix)
        landmarks_img = self.detect_special_markers(undist_frame)
        # landmarks_img_undist = self.undistort(landmarks_img)
        # landmarks_image = self.detect_special_markers(self._stored_frame)
        landmarks_world = self.image_to_world(landmarks_img)
        start_pose = self.get_robot_pose()
        fpath = self.detect_path(landmarks_world, start_pose, segment_origin)
        
        #DEBUG
        spath = SPath(landmarks_world)
        spath.rotate(start_pose.orientation)
        spath.shift(start_pose.position)
        with_keypoints = self.draw_points(undist_frame,
            self.arr_to_tuple_list(landmarks_img))
        self._keypoint_img_pub_debug.publish(with_keypoints)
        self.publish_points_rviz(spath.path)
        #/DEBUG

        fpath.smooth(0.08, step=self._path_step)
        return fpath
    

    def detect_curve_points(self, frame : Image):
        img = self._bridge.imgmsg_to_cv2(frame, 'bgr8')

        undist_frame = cv.undistort(img, self._cam_matrix,
            self._cam_info.D, None, self._cam_matrix)

        edges = cv.Canny(undist_frame, 100, 200)

        step = 50
        points = []
        for y in range(0, edges.shape[0], step):
            for x in range(0, edges.shape[1], step):
                detected = False
                for sub_y in range(y, min(y + step, edges.shape[0])):
                    for sub_x in range(x, min(x + step, edges.shape[1])):
                        if edges[sub_y][sub_x] > 0:
                            points.append(
                                [x + 0.5 * step,
                                 y + 0.5 * step]
                            )
                            detected = True
                            break
                    if detected:
                        break
        points_world = [self._image_projector.project(keypoint)
            for keypoint in points]
        points = [Point(x=pt[0], y=pt[1]) for pt in points_world]
        return points


    def detect_landmarks(self, data : Image) -> List[Point]:
        '''
        Returns landmark points in world coordinates (relative)
        '''
        img = self._bridge.imgmsg_to_cv2(data, 'bgr8')
        keypoints : List[cv.KeyPoint] = self._cv_blob_detector.detect(img)

        keypoints_list = keypoints_to_arr(keypoints)
        points_world = [self._image_projector.project(keypoint)
            for keypoint in keypoints_list]
        points_world = np.array([[point[0], point[1]] for point in points_world])
        # TODO: ???
        points = [Point(x=pt[0], y=pt[1]) for pt in points_world]
        return points
    

    def color_filter(self, image : np.ndarray, bounds : Tuple[List, List]):
        image_hsv = cv.cvtColor(image, cv.COLOR_BGR2HSV)

        # create NumPy arrays from the boundaries
        lower = np.array(bounds[0], dtype = "uint8")
        upper = np.array(bounds[1], dtype = "uint8")

        # find the colors within the specified boundaries and apply
        # the mask
        mask = cv.inRange(image_hsv, lower, upper)
        output = cv.bitwise_and(image_hsv, image_hsv, mask = mask)
        return cv.cvtColor(output, cv.COLOR_HSV2BGR)


    def image_to_world(self, points : List[Point]):
        points_world = [self._image_projector.project(point)
            for point in points]
        points = [Point(x=pt[0], y=pt[1]) for pt in points_world]
        return points


    def detect_colored_landmarks(self, data : Image) -> List[Point]:
        image = self._bridge.imgmsg_to_cv2(data, 'bgr8')
        
        undist_frame = cv.undistort(image, self._cam_matrix,
            self._cam_info.D, None, self._cam_matrix)
        filtered_image = self.color_filter(undist_frame, self._blob_color_bounds)
        # show the images
        # image_gray = cv.cvtColor(filtered_image, cv.COLOR_HSV2GRAY)
        _, _, image_gray = cv.split(filtered_image)
        keypoints : List[cv.KeyPoint] = self._cv_blob_detector.detect(image_gray)

        keypoints_list = keypoints_to_arr(keypoints)
        points_world = [self._image_projector.project(keypoint)
            for keypoint in keypoints_list]
        points_world = np.array([[point[0], point[1]] for point in points_world])
        # TODO: ???
        points = [Point(x=pt[0], y=pt[1]) for pt in points_world]
        return points
    

    def detect_landmarks_image(self, image : np.ndarray) -> List[Tuple[int]]:
        # image = self._bridge.imgmsg_to_cv2(data, 'bgr8')
        
        # undist_frame = cv.undistort(image, self._cam_matrix,
        #     self._cam_info.D, None, self._cam_matrix)
        filtered_image = self.color_filter(image, self._blob_color_bounds)
        # show the images
        # image_gray = cv.cvtColor(filtered_image, cv.COLOR_HSV2GRAY)
        # _, _, image_gray = cv.split(filtered_image)
        keypoints : List[cv.KeyPoint] = self._cv_blob_detector.detect(filtered_image)

        keypoints_list = keypoints_to_arr(keypoints)
        return keypoints_list
        # return [(int(point[0]), int(point[1])) for point in keypoints_list]
    

    def arr_to_tuple_list(self, arr : np.ndarray):
        return [(int(point[0]), int(point[1]))
            for point in arr]


    def detect_path(self, points : List[Point], start_pose: Pose,
            segment_origin : Ray = Ray()) -> FPath:
        '''
        Returns FPath in absolute coordinates
        '''
        # points, shift = self.move_origin(points, segment_origin)
        points = [point for point in points if self._sfag._rect.contains(point)]

        section = self._sfag.max_connected_section(copy.deepcopy(points), 0.3, self._path_scale)
        fpath = self._sfag.build_graph(section['cells'])
        fpath._source_points = points # debug
        fpath.start_point = start_pose.position

        # fpath.rotate(segment_origin.direction)
        # fpath.shift_path(shift)

        fpath.rotate(start_pose.orientation)
        fpath.shift_path(start_pose.position)
        if len(fpath.path) > 0:
            self._nearest_point_debug = fpath.path[0]
        return fpath


    def get_origin(self, spath: SPath) -> Ray:
        robot_pose = self.get_robot_pose()
        spath.shift(-robot_pose.position)
        spath.rotate(-robot_pose.orientation)
        radius = self._sfag._rect.height * 0.7
        center = self._sfag._rect.center

        if (len(spath.path) <= 1 or
                (spath.path[0] - center).magnitude2D() < radius):
            return Ray()

        for point in spath.path[1:]:
            if (point - center).magnitude2D() < radius:
                return Ray(
                    point,
                    center
                )
        # Path does not enter fow
        return Ray(spath.path[-1], center)


    def move_origin(self, points : List[Point],
            segment_origin : Ray):
        if len(points) == 0:
            return points, Point()
        nearest_point = min(
            points,
            key=lambda point : (segment_origin.origin - point).magnitude2D()
        )
        shifted_points = [point - nearest_point for point in points]
        rotated_shifted_points = [
            my_math.rotate_point(
                Point(), point, 
                -segment_origin.direction
            )
            for point in shifted_points
        ]
        return rotated_shifted_points, nearest_point


    def get_robot_pose(self) -> Pose:
        while True:
            try:
                self._tf_listener.waitForTransform(self._robot_frame,
                    self._base_link_frame,
                    self._tf_listener.getLatestCommonTime(self._base_link_frame,
                        self._robot_frame),
                    rospy.Duration(0)
                )
                break
            except Exception:
                rospy.logdebug('PATH_DETECTOR: Waiting for robot pose...')
                rospy.sleep(rospy.Duration(0.1))

        trans, q = self._tf_listener.lookupTransform(self._robot_frame,
            self._base_link_frame, self._tf_listener.getLatestCommonTime(self._base_link_frame,
                self._robot_frame))
        return Pose(position=Point(
                        x = trans[0],
                        y = trans[1]
                    ),
                    orientation=Quaternion(
                        x = q[0], y = q[1],
                        z = q[2], w = q[3]
                    )
               )


    def save_frame_callback(self, frame : Image):
        self._stored_frame = frame


    def draw_detection(self, img : np.ndarray) -> Image:
        keypoints : List[cv.KeyPoint] = self._cv_blob_detector.detect(img)
        cv.drawKeypoints(img, keypoints, img, flags = cv.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
        return self._bridge.cv2_to_imgmsg(img, 'bgr8')


    def rviz_publisher_debug(self, data):
        img = self._bridge.imgmsg_to_cv2(self._stored_frame, 'bgr8')
        undist_frame = cv.undistort(img, self._cam_matrix,
            self._cam_info.D, None, self._cam_matrix)
        # filtered_image = self.color_filter(undist_frame, self._blob_color_bounds)
        # landmarks_img_undist = self.undistort(landmarks_img)
        # landmarks_img = self.detect_landmarks_image(undist_frame)
        landmarks_img = self.detect_special_markers(undist_frame)
        landmarks_world = self.image_to_world(landmarks_img)
        robot_pose = self.get_robot_pose()
        spath = SPath(landmarks_world)
        spath.rotate(robot_pose.orientation)
        spath.shift(robot_pose.position)

        
        # filtered = self.color_filter(undist_frame, self._blob_color_bounds)
        # filtered = self._bridge.cv2_to_imgmsg(self.harris_filter(self._stored_frame), 'mono8')
        with_keypoints = self.draw_points(undist_frame,
            self.arr_to_tuple_list(landmarks_img))

        # edges = cv.Canny(img, 100, 200)
        # self._keypoint_img_pub_debug.publish(self._bridge.cv2_to_imgmsg(edges, 'mono8')) # DEBUG
        self._keypoint_img_pub_debug.publish(with_keypoints)
        self.publish_points_rviz(spath.path)


    def pub_frame_callback1(self, data):
        img = self._bridge.imgmsg_to_cv2(self._stored_frame, 'bgr8')

        undist_frame = cv.undistort(img, self._cam_matrix,
            self._cam_info.D, None, self._cam_matrix)
        with_keypoints = self.draw_detection(undist_frame)

        edges = cv.Canny(img, 100, 200)
        self._keypoint_img_pub_debug.publish(Image(data=edges))


    def draw_points(self, image : np.ndarray, points : List[Tuple[int, int]]):
        for point in points:
            image = cv.circle(image, point,
                8, (255, 0, 0), 2)
        return self._bridge.cv2_to_imgmsg(image, 'bgr8')


    def publish_points_rviz(self, points : List[Point],
            color : ColorRGBA = ColorRGBA(r=1, g=0, b=0, a=1),
            namespace = 'eyefield'):
        try:
            markers = [self.create_simple_marker(i, point, color=color, namespace=namespace, scale=1)
                    for i, point in enumerate(points, 1)]
        except IndexError:
            markers = []
        self._rviz_pub_debug.publish(MarkerArray(markers = markers))


    def fork_publisher(self, data):
        self.publish_points_rviz(self._forks)

    
    def create_simple_marker(self, id : int, point : Point, color : ColorRGBA,
                             scale : float = 1.6, namespace : str = 'world') -> Marker:
        return Marker(
            header = Header(
                stamp = rospy.Time.now(),
                frame_id = 'map'
            ), 
            ns = namespace, id = id,
            type = 2, action = 0,
            pose = Pose(position = point),
            scale = Vector3(
                x = 0.05 * scale,
                y = 0.05 * scale,
                z = 0.05 * scale
            ),
            color = color,
            lifetime = rospy.Duration(1.5)
        )


    def undistort(self, points : np.ndarray) -> List[Tuple[int, int]]:
        if points.size == 0:
            return []
        undist = cv.undistortPoints(points, self._distorted_cam_matrix, self._cam_info.D)
        undist_pixels = [point[0] * self._f + self._c for point in undist]
        undist_pixels_tuples = [(int(point[0]), int(point[1]))
            for point in undist_pixels]
        return undist_pixels_tuples