import argparse

import cv2
import numpy as np
import rospy

from cv_bridge import CvBridge
from sensor_msgs.msg import Image


br = CvBridge()


def frame_callback(data : Image):
    image = br.imgmsg_to_cv2(data, 'bgr8')
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    image_hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

    # create NumPy arrays from the boundaries
    lower = np.array([23, 10, 0], dtype = "uint8")
    upper = np.array([80, 255, 255], dtype = "uint8")

    # find the colors within the specified boundaries and apply
    # the mask
    mask = cv2.inRange(image_hsv, lower, upper)
    output = cv2.bitwise_and(image_hsv, image_hsv, mask = mask)
    # show the images
    image_bgr = cv2.cvtColor(output, cv2.COLOR_HSV2BGR)
    cv2.imshow("images", np.hstack([image, image_bgr]))
    cv2.waitKey(0)


def main():
    rospy.init_node('color_filter_test')
    rospy.Subscriber('/yarp8r2/sensors/camera/image_raw', Image, frame_callback, queue_size=1)
    rospy.spin()


if __name__ == '__main__':
    main()