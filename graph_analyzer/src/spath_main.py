from typing import List

import rospy
from geometry_msgs.msg import Point, Pose, PoseStamped
from nav_msgs.msg import Path
from std_msgs.msg import Header

import my_math
import rostools
from fpath import FPath
from spath import SPath


class SPathMain(SPath):
    MAX_LEN = 50 
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.STEP : float = kwargs['step']
        self.TOLERANCE : float = kwargs['tolerance']
        self.SMOOTH_SCALE : float = kwargs['smooth_scale']
        self.TURN_STEP : float = kwargs['turn_step']


    @property
    def path_gmsgs(self):
        poses = self.orient_path(self._path)
        poses_stamped = [
            PoseStamped(
                pose=pose,
                header=Header(
                    stamp=rospy.Time.now()
                )
            )
            for pose in poses
        ]
        return Path(poses=poses_stamped)


    def orient_path(self, path : List[Point]) -> List[Pose]:
        new_path = [Pose(position=point) for point in path]
        if len(path) <= 1:
            return new_path
        for i in range(1, len(new_path)):
            segment = new_path[i].position - new_path[i-1].position
            segment_orientation = my_math.vector_quaternion2D(segment)
            new_path[i].orientation = segment_orientation
        new_path[0].orientation = new_path[1].orientation
        return new_path


    def build_on(self, fpath : FPath, turn = None,
            replace=False):
        if turn is not None:
            segment = fpath.with_fork(turn, self.TURN_STEP)
        else:
            segment = fpath.path
        
        if len(self._path) == 0:
            self._path = [fpath.start_point]

        success = self.insert_segment(segment, replace=True)

        if len(self) > self.MAX_LEN:
            self._path = self._path[(-self.MAX_LEN):]
        
        return success
    

    def insert_segment(self, segment: SPath, replace=False):
        # Логика такая: вставляем сегмент посерёдке пути.
        # Но если длина заменяемого сегмента малеькая либо
        # отрицательная, делаем вывод, что путь слегка заг-
        # нулся. В этом случае требуется вставить путь без
        # загнувшегося хвоста.
        if len(segment) == 0:
            return True
        start_index = self.nearest_point_index(segment[0])
        end_index = self.nearest_point_index(segment[-1])

        if (self.path[start_index] - segment[0]).magnitude2D() > self.TOLERANCE:
            print('Build on failed')
            return False
        segment_to_replace = SPath(self[(start_index + 1):(end_index + 1)])
        normal_insert = (segment_to_replace.calculate_length() > 0.5 * segment.calculate_length() and
            start_index < end_index)
        if replace:
            normal_insert = False
        if normal_insert:
            print('DEBUG start', start_index, 'end', end_index, 'len', len(self), '')
            self._path = self.path[:(start_index + 1)] + segment.path + self.path[(end_index + 1):]
        else:
            print('DEBUG not normal', start_index, 'end', end_index, 'len', len(self), '')
            self._path = self.path[:(start_index + 1)] + segment.path 
        self.smooth(self.STEP, self.SMOOTH_SCALE)

        return True


    def nearest_point_index(self, init_point: Point):
        nearest = min(
            enumerate(self.path),
            key=lambda point : (init_point - point[1]).magnitude2D()
        )
        return nearest[0]
