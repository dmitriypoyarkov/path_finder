from geometry_msgs.msg import Point
import numpy as np

class Rect(object):
    def __init__(self, top_left : Point, bottom_right : Point, flipped : bool = False):
        self._top_left = top_left
        self._bottom_right = bottom_right
        self._flipped = flipped

        self._inversedY = True if top_left.y - bottom_right.y < 0 else False
        # width = Y, height = X
        self._height = abs(top_left.x - bottom_right.x)
        self._width = abs(top_left.y - bottom_right.y)

        if self._height == 0 or self._width == 0:
            raise Exception('Rect: Rectangle area is zero!')

        if not flipped:
            self._bottom = Point(x=(self._bottom_right.x + self._top_left.x) / 2,
                                y=self._bottom_right.y if self._inversedY
                                else self._top_left.y)
        else:
            self._bottom = Point(x=min(self._bottom_right.x, self._top_left.x),
                                y=np.average([self._bottom_right.y, self._top_left.y]))

        self._center = Point(
            x=self._bottom.x + self._height / 2,
            y=self._bottom.y
        )


    @property
    def center(self):
        return self._center


    @property
    def height(self):
        return self._height

    @property
    def width(self):
        return self._width

    @property
    def top_left(self):
        return self._top_left
    
    @property
    def bottom_right(self):
        return self._bottom_right
    
    @property
    def bottom(self):
        return self._bottom
    
    @property
    def flipped(self):
        return self._flipped
    
    def contains(self, point : Point) -> bool:
        diff1_vertical = self._top_left.y - point.y
        diff2_vertical = self._bottom_right.y - point.y
        if np.sign(diff1_vertical) == np.sign(diff2_vertical):
            return False
        diff1_horizontal = self._top_left.x - point.x
        diff2_horizontal = self._bottom_right.x - point.x
        if np.sign(diff1_horizontal) == np.sign(diff2_horizontal):
            return False
        return True