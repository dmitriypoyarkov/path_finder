from graph_analyzer.msg import PathFinderState

class State(object):
    def __init__(self,
            id : int,
            idle=False,
            getpath_callback = False,
            fork_callback = False,
            fork_move = False,
            fork_visible = False,
            search=False,
            obstacle_move=False,
            start=False,
            path_follow=False,
            finish=False,
            directing=False,
            lost=False,
            previous_state=None):

        self._id = id
        self.idle = idle
        self.getpath_callback = getpath_callback
        self.fork_callback = fork_callback
        self.fork_move = fork_move
        self.fork_visible = fork_visible
        self.search = search
        self.obstacle_move=obstacle_move
        self.start=start
        self.path_follow=path_follow
        self.finish=finish
        self.directing=directing
        self.lost = lost

        self._previous_state = previous_state


    @property
    def id(self):
        return self._id
    
    
    @property
    def previous_state(self):
        if not self._previous_state:
            raise Exception('No previous state provided for this state!')
        return self._previous_state


class StateMachine():
    def IDLE():
        return State(PathFinderState.IDLE, idle=True)
    def START():
        return State(PathFinderState.START, start=True)
    def PATH_FOLLOW():
        return State(PathFinderState.PATH_FOLLOW, getpath_callback=True,
            path_follow=True)
    def FORK_MOVE():
        return State(PathFinderState.FORK_MOVE, getpath_callback=True,
            fork_move=True, path_follow=True, fork_visible=True)
    def FORK_CHOICE_WAIT():
        return State(PathFinderState.FORK_CHOICE_WAIT, fork_callback=True,
            fork_visible=True)
    def SEARCH(previous_state):
        return State(PathFinderState.SEARCH, getpath_callback=True,
            search=True, previous_state=previous_state)
    def OBSTACLE_MOVE():
        return State(PathFinderState.OBSTACLE_MOVE, getpath_callback=True,
            obstacle_move=True)
    def FINISH(previous_state):
        return State(PathFinderState.FINISH, finish=True,
            previous_state=previous_state)
    def DIRECTING(previous_state):
        return State(PathFinderState.DIRECTING, getpath_callback=True,
            directing=True, previous_state=previous_state)
    def LOST():
        return State(PathFinderState.LOST, lost=True)