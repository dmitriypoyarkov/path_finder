import rospy
from rospy import Header
from actionlib import SimpleActionClient
from graph_analyzer.srv import ForkChoice, ForkChoiceResponse, ForkChoiceRequest
from graph_analyzer.msg import PathFinderActionGoal, PathFinderAction, PathFinderGoal
from std_msgs.msg import UInt8


def fork_choice_callback(req : UInt8):
    global fork_pub
    print('Выберите развилку из числа:', req.data)
    index = int(input())
    fork_pub.publish(UInt8(data=index))


def main():
    global fork_pub
    rospy.init_node('pf_client', log_level=rospy.DEBUG)
    fork_pub = rospy.Publisher('/global_planner_service_node/fork_choice', UInt8, queue_size=2)
    fork_sub = rospy.Subscriber('/pf_client_sub', UInt8, fork_choice_callback, queue_size=2)
    action_client = SimpleActionClient('/global_planner_service_node/path_finder', PathFinderAction)
    print("Waiting...")
    action_client.wait_for_server()
    print("Ok!...")

    action_client.send_goal(PathFinderGoal(
            fork_choice_mode=PathFinderGoal.USER_CHOICE
        )
    )
    rospy.spin()


if __name__ == '__main__':
    main()