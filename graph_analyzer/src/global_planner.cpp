#include <pluginlib/class_list_macros.h>
#include <tf/transform_broadcaster.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <tf2_ros/transform_listener.h>
#include <cstring>
#include <math.h>

#include "graph_analyzer/global_planner.h"
#include "graph_analyzer/GetPath.h"
#include "graph_analyzer/MyMath.h"

//register this planner as a BaseGlobalPlanner plugin
PLUGINLIB_EXPORT_CLASS(global_planner::GlobalPlanner, nav_core::BaseGlobalPlanner)

//Default Constructor
namespace global_planner
{
    GlobalPlanner::GlobalPlanner() : initialized_(false)
    {
    }

    GlobalPlanner::GlobalPlanner(std::string name, costmap_2d::Costmap2D *costmap, std::string frame_id) : GlobalPlanner()
    {
        initialize(name, costmap, frame_id);
    }

    void GlobalPlanner::initialize(std::string name, costmap_2d::Costmap2DROS *costmap_ros)
    {
        initialize(name, costmap_ros->getCostmap(), costmap_ros->getGlobalFrameID());
    }

    void GlobalPlanner::initialize(std::string name, costmap_2d::Costmap2D *costmap, std::string frame_id)
    {
        if (!initialized_)
        {
            ros::NodeHandle private_nh("~/" + name);
            frameId_ = frame_id;
            planPub_ = private_nh.advertise<nav_msgs::Path>("plan", 1);
            planClient_ = private_nh.serviceClient<graph_analyzer::GetPath>("/global_planner_service");
            this->markerPub_ = private_nh.advertise<visualization_msgs::Marker>("plan_orientation", 1);
            std::string s = planClient_.getService();
            char p[s.length()];

            for (int i = 0; i <= sizeof(p); i++)
            {
                p[i] = s[i];
            }
            ROS_DEBUG("Global Planner: inited! Service %s", p);

            initialized_ = true;
        }
        else
        {
            ROS_WARN("This planner has already been initialized!");
        }
    }

    bool GlobalPlanner::makePlan(const geometry_msgs::PoseStamped &start,
                                 const geometry_msgs::PoseStamped &goal,
                                 std::vector<geometry_msgs::PoseStamped> &plan)
    {
        auto response = graph_analyzer::GetPath::Response();
        auto request = graph_analyzer::GetPath::Request();

        planClient_.call(request, response);
        auto newPlan = response.path.poses;
        for (auto pose : newPlan)
        {
            pose.header.frame_id = frameId_;
            pose.header.stamp = ros::Time::now();
            plan.push_back(pose);
        }
        this->publishPlan(plan, planPub_);
        return true;
    }

    void GlobalPlanner::publishPlan(const std::vector<geometry_msgs::PoseStamped> &path, const ros::Publisher &pub)
    {
        if (!initialized_)
        {
            ROS_ERROR(
                "This planner has not been initialized yet, but it is being used, please call initialize() before use");
            return;
        }
        nav_msgs::Path gui_path;
        gui_path.poses.resize(path.size());
        gui_path.header.frame_id = frameId_;
        gui_path.header.stamp = ros::Time::now();

        // Extract the plan in world co-ordinates, we assume the path is all in the same frame
        for (unsigned int i = 0; i < path.size(); i++)
        {
            gui_path.poses[i] = path[i];
            gui_path.poses[i].header.frame_id = frameId_;
        }
        pub.publish(gui_path);

        for (auto it = path.begin(); it != path.end(); ++it)
        {
            auto marker = visualization_msgs::Marker();
            marker.header.frame_id = frameId_;
            marker.header.stamp = ros::Time::now();
            marker.type = visualization_msgs::Marker::ARROW;
            marker.ns = "world";
            marker.pose = it->pose;
            marker.color.a = 1.0;
            marker.color.r = 1.0;
            marker.color.g = 1.0;
            marker.color.b = 0;
            marker.id = 1000 + std::distance(path.begin(), it);
            marker.scale.x = 0.7;
            marker.scale.y = 0.15;
            marker.scale.z = 0.15;
            marker.lifetime = ros::Duration(0.1);
            this->markerPub_.publish(marker);
        }
    }
}