import json
from json.decoder import JSONDecodeError
from typing import Dict, List, Type, Union

from geometry_msgs.msg import Point
from rospy_message_converter.json_message_converter import \
    convert_json_to_ros_message as to_ros
from rospy_message_converter.json_message_converter import \
    convert_ros_message_to_json as to_json
from sensor_msgs.msg import CameraInfo

from fpath import FPath
from rect import Rect


class DebugManager(object):
    def __init__(self, data_file = '', cam_info_file = '', rect_file = ''):
        self.POINTS_FILE = data_file
        self.CAM_INFO_FILE = cam_info_file
        self.RECT_FILE = rect_file

        print('DEBUG MANAGER: Inited, output:', data_file)

        self._cur_data = {}

    
        


    def load_or_create_json(self, path : str, json_type : Type) -> Union[Dict, List]:
        while True:
            try:
                with open(path, 'a+') as f:
                    f.seek(0,0)
                    data = json.load(f)
                    return data
            except JSONDecodeError as e:
                with open(path, 'w') as f:
                    if json_type == Dict:
                        f.write('{}')
                    if json_type == List:
                        f.write('[]')        


    def save_rect(self, rect : Rect, frame_topic : str):
        data = self.load_or_create_json(self.RECT_FILE, Dict)
        data[frame_topic] = [to_json(rect.top_left), to_json(rect.bottom_right), rect.flipped]
        with open(self.RECT_FILE, 'w') as f:
            json.dump(data, f)


    def load_rect(self, frame_topic : str):
        data = self.load_or_create_json(self.RECT_FILE, Dict)
        
        return Rect(
            to_ros('geometry_msgs/Point', data[frame_topic][0]),
            to_ros('geometry_msgs/Point', data[frame_topic][1]),
            data[frame_topic][2]
        )


    def save_cam_info(self, cam_info : CameraInfo, cam_topic : str):
        data = self.load_or_create_json(self.CAM_INFO_FILE, Dict)
        data[cam_topic] = to_json(cam_info)
        with open(self.CAM_INFO_FILE, 'w') as f:
            json.dump(data, f)
    

    def load_cam_info(self, cam_topic : str) -> CameraInfo:
        data = self.load_or_create_json(self.CAM_INFO_FILE, Dict)
        return to_ros('sensor_msgs/CameraInfo', data[cam_topic])

    
    def add_fpath(self, fpath : FPath):
        path_buf = [to_json(x) for x in fpath.path]
        fork_buf = [to_json(x) for x in fpath.fork_steps]
        source_buf = [to_json(x) for x in fpath._source_points]
        self._cur_data['fpath'] = {
            'path' : path_buf,
            'forks' : fork_buf,
            'source' : source_buf
        }
    

    def add_full_path(self, path : List[Point]):
        path_buf = [to_json(x) for x in path]
        self._cur_data['full_path'] = path_buf
    

    def add_new_path(self, path : List[Point]):
        path_buf = [to_json(x) for x in path]
        self._cur_data['new_path'] = path_buf
    
    def push(self):
        data = self.load_or_create_json(self.POINTS_FILE, List)

        data.append(self._cur_data)
        with open(self.POINTS_FILE, 'w') as f:
            json.dump(data, f)


    def load_data(self):
        data = self.load_or_create_json(self.POINTS_FILE, Dict)

        for k, item in enumerate(data):
            if item.get('fpath'):
                for i, point in enumerate(item['fpath']['path']):
                    data[k]['fpath']['path'][i] = to_ros('geometry_msgs/Point', point)
                for i, point in enumerate(item['fpath']['forks']):
                    data[k]['fpath']['forks'][i] = to_ros('geometry_msgs/Point', point)
                for i, point in enumerate(item['fpath']['source']):
                    data[k]['fpath']['source'][i] = to_ros('geometry_msgs/Point', point)
            for i, point in enumerate(item['full_path']):
                data[k]['full_path'][i] = to_ros('geometry_msgs/Point', point)
            for i, point in enumerate(item['new_path']):
                data[k]['new_path'][i] = to_ros('geometry_msgs/Point', point)
        return data
