import math
from tracemalloc import start
from typing import List, Tuple, Union, overload

import numpy as np
from geometry_msgs.msg import Point, Quaternion
from graph_analyzer.msg import PathFinderGoal
from scipy import interpolate

import my_math
import rostools
from cell import Cell

class SPath(object):
    @overload
    def __init__(self): ...
    @overload
    def __init__(self, points : List[Point]): ...


    def __init__(self, *args, **kwargs):
        if len(args) == 1:
            self._path = args[0]
        else:
            self._path : List[Point] = kwargs.get('points', [])


    def __getitem__(self, i):
        return self._path[i]


    def __len__(self):
        return len(self._path)


    def __add__(self, other : "SPath"):
        return SPath(self.path + other.path)
    

    @property
    def path(self):
        return self._path


    @path.setter
    def path(self, value):
        self._path = value


    def calculate_length(self):
        if len(self._path) <= 1:
                return 0.0
        length = 0.0
        for i in range(1, len(self._path)):
            length += (self._path[i] - self._path[i - 1]).magnitude2D()
        return length


    def mill_path(self, path : List[Point]) -> List[Point]:
        if len(path) <= 1:
            return path
        new_path : List[Point] = [path[0]]
        for i in range(1, len(path)):
            average_point = my_math.average2D([path[i-1], path[i]])
            new_path.append(average_point)
            new_path.append(path[i])
        return new_path


    def smooth(self, step : float, scale = 0.05):
        length = self.calculate_length()
        if length <= step * 4:
            return
        while len(self._path) <= 4:
            self._path = self.mill_path(self._path)
        weights = np.ones(len(self._path))
        weights[0] = 5
        weights[-1] = 2.5
        tck, u = interpolate.splprep([[pt.x for pt in self._path],
            [pt.y for pt in self._path]], s=scale, w=weights)
        nodes_count = math.ceil(length / step)
        result = interpolate.splev(np.linspace(0, 1, nodes_count), tck)
        self.path = [Point(x=x, y=y) for x, y in zip(result[0], result[1])]


    @overload
    def rotate(self, orientation : Quaternion): ...
    @overload
    def rotate(self, yaw : float): ...

    def rotate(self, rotation : Union[Quaternion, float]):
        if isinstance(rotation, Quaternion):
            orientation = rotation
        elif isinstance(rotation, (float, int)):
            orientation = my_math.quaternion_from_euler(0, 0, rotation)
            orientation = Quaternion(x=orientation[0], y=orientation[1],
                z=orientation[2], w=orientation[3])
        self.path = [my_math.rotate_2d_point_gmsgs(point, orientation)
                     for point in self._path]


    def shift(self, shift : Point):
        self.path = [point + shift for point in self._path]

    
    def mill(self):
        if len(self) <= 1:
            return
        new_path : List[Point] = [self[0]]
        for i in range(1, len(self)):
            average_point = my_math.average2D([self[i-1], self[i]])
            new_path.append(average_point)
            new_path.append(self[i])
        self._path = new_path
    

    def continues(self, other_path : "SPath", tolerance : float,
            required_percent=0.225, acceptable_deviation=0.00, strict=False):
        # strict continuation = at least reach the end of
        # existing path

        #TODO: Multiple contact segments case;
        #TODO: Refactor
        if len(self.path) == 0:
            return False
        if len(other_path) == 0:
            return True
        distances_to_start = [
            (other_point - self[0]).magnitude2D()
            for other_point in other_path
        ]
        # i_varname stands for tuple (index, variable) from enumerate
        i_first_contact = min(
            enumerate(distances_to_start),
            key=lambda i_distance : i_distance[1]
        )
        if i_first_contact[1] >= tolerance:
            return False
        first_contact_index = i_first_contact[0]

        contact_segment = SPath(other_path[first_contact_index:])
        distances_from_self = [
            min([(other_point - self_point).magnitude2D()
            for self_point in self])
            for other_point in contact_segment
        ]
        contacts_with_self = [distance < tolerance
            for distance in distances_from_self]

        try:
            first_deviation = contacts_with_self.index(False)
            deviation_segment = SPath(contact_segment[first_deviation:])
            deviation_length = deviation_segment.calculate_length()
            if deviation_length > acceptable_deviation:
                return False
            if not strict:
                return True
            distances_from_other = [
                min([(other_point - self_point).magnitude2D()
                for other_point in contact_segment])
                for self_point in self
            ]
            contacts_with_other = [distance < tolerance
                for distance in distances_from_other]
            try:
                contacts_with_other.index(False)
                print('DEBUG strict continuation', self.path, other_path.path)
                return True
            except ValueError:
                return False
        except ValueError:
            if strict:
                # check for shortening
                if ((other_path[-2] - other_path[-1]).magnitude2D() >
                        (self[-1] - other_path[-2]).magnitude2D()):
                    print('DEBUG strict, not shortening')
                    return False
            if len(contact_segment) < required_percent * len(self):
                return False
            return True
        return False