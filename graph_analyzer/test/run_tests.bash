# for name_of_set in "set2_forked" # "set3_noised" "set4_noised_forked"
# for name_of_set in "set1_simple" # "set2_simple_forked" "set3_noised" "set4_noised_forked"
# for name_of_set in "set5_noised_forked_obstacles"
for name_of_set in "set2_forked" # "set3_noised" "set4_noised_forked" "set3_noised"
do
    for num in {1..5}
    do
        cur_world="/home/fc/catkin_ws/src/path_finder/graph_analyzer/test/$name_of_set/world/$num.world"
        # echo CUR WORLD IS $cur_world
        roslaunch yarp8_config yarp8_gazebo_empty.launch world:=$cur_world localization_method:=STATIC &
        # roslaunch yarp8_config yarp8_gazebo_empty.launch &
        # sleep 40
        pid=$!
        python3 test_client.py "./$name_of_set/json/$num.json"
        kill $pid
        sleep 5
    done
done