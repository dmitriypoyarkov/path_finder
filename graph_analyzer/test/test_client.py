import json
import sys
import pathlib

import numpy
import rospy
from actionlib import SimpleActionClient
from geometry_msgs.msg import Point, PointStamped
from graph_analyzer.msg import (PathFinderAction, PathFinderActionGoal,
                                PathFinderGoal, PathFinderResult)
from graph_analyzer.srv import (ForkChoice, ForkChoiceRequest,
                                ForkChoiceResponse)
from rospy import Header
from std_msgs.msg import UInt8

GOAL_TOLERANCE = 0.3
OUTPUT_FILE = 'test_result.json'

def fork_choice_callback(req : UInt8):
    global fork_pub, fork_seq, fork_seq_num
    rospy.sleep(2)
    fork_pub.publish(UInt8(data=fork_seq[fork_seq_num]))
    fork_seq_num += 1


def motion_finish_callback(msg : PointStamped):
    global goal_coordinate

    
def main():
    global fork_pub, fork_seq, fork_seq_num, goal_coordinate
    with open(sys.argv[1], 'r') as f:
        data = json.load(f)
    goal_coordinate = data['goal']
    fork_seq = data['fork_seq']
    fork_seq_num = 0

    rospy.init_node('pf_client', log_level=rospy.DEBUG)
    fork_pub = rospy.Publisher('/global_planner_service_node/fork_choice', UInt8, queue_size=2)
    fork_sub = rospy.Subscriber('/pf_client_sub', UInt8, fork_choice_callback, queue_size=2)

    action_client = SimpleActionClient('/global_planner_service_node/path_finder', PathFinderAction)
    action_client.wait_for_server()
    action_client.send_goal(PathFinderGoal(
            fork_choice_mode=PathFinderGoal.USER_CHOICE
        )
    )
    rospy.sleep(1)
    pub = rospy.Publisher('/global_planner_service_node/path_finder/goal', PathFinderActionGoal, queue_size=2)
    pub.publish(PathFinderActionGoal(
            header=Header(
                stamp=rospy.Time.now(),
                frame_id='map'
            ),
            goal=PathFinderGoal(
                fork_choice_mode=PathFinderGoal.USER_CHOICE
            )
        )
    )
    print('Клиент: жду результат')
    action_client.wait_for_result()
    result : PathFinderResult = action_client.get_result()
    print('Клиент: получил результат')
    final_position = result.position.point
    goal_diff = ((final_position.x - goal_coordinate[0])**2 + 
        (final_position.y - goal_coordinate[1])**2)**0.5
    goal_reached = goal_diff < GOAL_TOLERANCE
    with open(OUTPUT_FILE, 'r') as f:
        try:
            data = json.load(f)
        except json.decoder.JSONDecodeError as e:
            print('Джейсон опять недоволен: ', e.msg)
            data = {}
    data[sys.argv[1]] = goal_reached
    with open(OUTPUT_FILE, 'w') as f:
        json.dump(data, f)
    print('TEST_CLIENT: Final position:', sys.argv[1], final_position)
    exit()


if __name__ == '__main__':
    main()