# Path Finder

## Описание
ROS-пакет, реализующий интерфейс глобального планировщика для move_base (http://wiki.ros.org/move_base) для следования вдоль цепочки визуальных ориентиров.
## Установка
1. Клонировать репозиторий
``` bash
~/your_ws/src
git clone https://gitlab.com/dmitriypoyarkov/path_finder.git -b master1
```
## Использование
1. Для работы с _move_base_, в файле _move_base_params.yaml_ указать:
``` yaml
global_planner: graph_analyzer/GlobalPlanner
```
2. В launch-файл пакета, работающего под _move_base_, добавить следующую строку:
```xml
<node name="global_planner_service_node" pkg="graph_analyzer" type="main.py" output="screen"/>
```
3. После запуска launch-файла, система ожидает команду для старта. Команда - сообщение в топик /_global_planner_service_node/path_finder/goal_.

Скриншот из симуляции:  
<img src="graph_analyzer/img/path_finder.png" width=500>